// Import react first so we augment it instead of redefining it
// eslint-disable-next-line
import * as React from 'react'

declare module 'react' {
	// extends React's HTMLAttributes
	interface HTMLAttributes<T> extends AriaAttributes, DOMAttributes<T> {
		class?: string
	}
}
