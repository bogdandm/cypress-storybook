#!/usr/bin/env bash

set -x

# Clean up
rm -rf dist

# Compile
npm run tsc

# Copy styl files
rsync -aRvm --include="*/" --include="*.styl" --exclude="*" antd/ dist/
rsync -aRvm --include="*/" --include="*.styl" --exclude="*" components/ dist/
