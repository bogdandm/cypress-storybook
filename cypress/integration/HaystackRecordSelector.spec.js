/// <reference types="Cypress" />

import funct from '../support/functions'
import el from '../support/elements'


describe('Haystack Record Selector',()=>{
    beforeEach(()=>{
        cy.visit('/iframe.html?id=haystack-record-selector--basic');
    })
    it('The elements are displayed',()=>{
        cy.get(el.mainRoot).should('be.visible');

        cy.get(el.searchBox).should('be.visible')
          .find('svg').should('be.visible')
          .parent().parent().parent().find('input').should('be.visible')
          .parent().find('.ant-input-suffix').should('be.visible');

        cy.get(el.recordTree).should('be.visible')
        cy.get(el.recordTreeLine).children('[title="City Center"]').should('be.visible').siblings(el.expandCollapseButton).should('be.visible')
        cy.get(el.recordTreeLine).children('[title="JB Tower"]').should('be.visible').siblings(el.expandCollapseButton).should('be.visible')
        cy.get(el.recordTreeLine).children('[title="Laguna"]').should('be.visible').siblings(el.expandCollapseButton).should('be.visible')
        cy.get(el.recordTreeLine).children('[title="Malibu"]').should('be.visible').siblings(el.expandCollapseButton).should('be.visible')
        cy.get(el.recordTreeLine).children('[title="Pearl Tower"]').should('be.visible').siblings(el.expandCollapseButton).should('be.visible')
    })

    it('Expand and collapse testing',()=>{
        
        //expand site
        cy.get(el.recordTreeLine).children('[title="City Center"]').siblings(el.expandCollapseButton).click();
        for(var i = 1; i <= 4; i++)
            cy.get(el.recordTreeLine).children('[title="Floor ' + i + '"]').should('be.visible').siblings(el.expandCollapseButton).should('be.visible');
        cy.get(el.recordTreeLine).children('[title="Utility "]').should('be.visible').siblings(el.expandCollapseButton).should('be.visible');
        cy.get(el.recordTreeLine).children('[title="City Center"]').siblings(el.expandCollapseButton).should('have.class','ant-tree-switcher_open');
        
        //expand floor
        cy.get(el.recordTreeLine).children('[title="Floor 1"]').siblings(el.expandCollapseButton).click();
        cy.contains('City Center AHU-1').should('be.visible');
        for(var i = 1; i <= 5 ; i++)
            cy.contains('City Center Vav-0' + i).should('be.visible');
        cy.get(el.recordTree).scrollTo('center');
        for(var i = 6; i < 10 ; i++)
            cy.contains('City Center Vav-0' + i).should('be.visible');
        cy.contains('City Center Vav-10').should('be.visible');
        cy.get(el.recordTree).scrollTo('top');
        cy.get(el.recordTreeLine).children('[title="Floor 1"]').siblings(el.expandCollapseButton).should('have.class','ant-tree-switcher_open');

        //collapse floor
        cy.get(el.recordTreeLine).children('[title="Floor 1"]').siblings(el.expandCollapseButton).click()
        cy.contains('City Center AHU-1').should('not.exist');
        for(var i = 1; i <= 10; i++)
            cy.contains('City Center Vav-0' + i).should('not.exist');
        cy.get(el.recordTreeLine).children('[title="Floor 1"]').siblings(el.expandCollapseButton).should('have.class','ant-tree-switcher_close');

        //collapse site 
        cy.get(el.recordTreeLine).children('[title="City Center"]').siblings(el.expandCollapseButton).click();
        for(var i = 1; i<= 4; i++)
            cy.contains('Floor ' + i).should('not.exist')
        cy.contains('Utility').should('not.exist')
        cy.get(el.recordTreeLine).children('[title="City Center"]').siblings(el.expandCollapseButton).should('have.class','ant-tree-switcher_close');
    })

    it('SearchBox testing',()=>{
        
        const pageActive = 'ant-pagination-item-active';
        const pageNo = '.ant-pagination-item';
        const somethingRandom = funct.randomString(funct.getRandomInt(7));
        const someRandomLetter = funct.getRadonLetter();
        const someRandomOneDigitNumber = funct.getRandomInt(8);
        const randomPlusOrMinus = funct.randomPositiveOrNegative();

        //multiple result search
        cy.get(el.searchBox).find('input').type('point{enter}');
        cy.get(el.searchResultTable,{timeout:10000}).should('be.visible');
        cy.get(el.searchResultRow).contains('SAT').should('be.visible').find('i').should('be.visible');
        cy.get(el.searchResultRow).contains('AirFlow Setpoint').should('be.visible').find('i').should('be.visible');
        cy.get(el.searchResultRow).contains('Malibu, CA Humidity').should('be.visible').find('i').should('be.visible');
        cy.get(el.searchResultRow).contains('RAT').should('exist');
        cy.get(el.pagination).should('be.visible');
        cy.get(pageNo + '-1').should('have.class',pageActive);
        cy.get(pageNo + '-5').should('not.have.class',pageActive)
            .find('a').last().click();
        cy.get(pageNo + '-5').should('have.class',pageActive)   
        cy.contains(pageNo,'1').should('not.have.class',pageActive);
        cy.get(el.searchResultRow).contains('OccHeat').should('be.visible').find('i').should('be.visible')
        cy.get(el.searchResultRow).contains('Damper').should('exist');

        //One result search with a valid Axon query as input
        cy.get(el.searchBox).find(el.clearSearchBoxButton).should('be.visible').click();
        cy.get(el.searchBox).find('input').should('have.value','').type('dis == "City Center"{enter}');
        cy.get(el.searchResultTable,{timeout:10000}).should('be.visible');
        cy.get(el.searchResultRow).contains('City Center').should('be.visible').find('i').should('be.visible').click();
        cy.get(el.resultInfo).should('be.visible');
        cy.get(el.pagination).should('not.be.visible');

        //One result search with a random input (first character must be a number) 
        cy.get(el.searchBox).find(el.clearSearchBoxButton).should('be.visible').click();
        cy.get(el.searchBox).find('input').should('have.value','').type(randomPlusOrMinus + someRandomOneDigitNumber + somethingRandom + '{enter}');
        cy.get(el.searchResultTable,{timeout:10000}).should('be.visible');
        cy.get(el.searchResultRow).should('be.visible').should('have.text','').find('i').should('be.visible');

        //No result search with a random input (first character must be everything but a number)
        cy.get(el.searchBox).find(el.clearSearchBoxButton).click();
        cy.get(el.resultInfo).should('not.exist');
        cy.get(el.searchBox).find('input').should('have.value','').type(someRandomLetter + somethingRandom + '{enter}');
        cy.get(el.searchResultTable).should('not.exist');
        cy.get(el.pagination).should('not.exist');
        cy.contains('No results').should('be.visible');

        
    })

})