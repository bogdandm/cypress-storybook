/// <reference types="Cypress" />

describe('HaystackRecordEditor', () => {

	beforeEach(() => {
		cy.visit('/iframe.html?id=haystack-record-editor--standalone')
	})

	// https://on.cypress.io/interacting-with-elements

	it('should show editors', () => {
		cy.contains('id').should('exist')
		cy.contains('mod').should('exist')
		cy.contains('dis')
			.should('exist')
			.parent().parent()
			.contains('Laguna')
			.should('exist')
	})


	it('adding new tags', () => {
		const nameInput = '[data-test-id="new-tag-name-input"]'
		const valueInput = '[data-test-id="new-tag-value-input"]'
		const addButton = '[data-test-id="new-tag-add-button"]'

		cy.get(nameInput).type('testTag1')
		cy.get(addButton).click()
		cy.contains('testTag1')

		cy.get(nameInput).type('testTag2')
		cy.get(valueInput).type('testTag2Value{enter}')
		cy.contains('testTag2')
			.parent().parent()
			.contains('testTag2Value')

	})

	it('renaming tags', () => {
		cy.contains('dis')
			.parent()
			.children('i')
			.click()
		cy.get('body')
			.contains('Rename')
			.click()
		cy.get('[data-test-id="hre-tag-rename-input"]')
			.type('newTagName{enter}')
		cy.contains('newTagName')

	})

	it('deleting tags', () => {
		cy.contains('dis')
			.parent()
			.children('i')
			.click()
		cy.get('body')
			.contains('Delete')
			.click()
		cy.contains('newTagName')
			.should('not.exist')

	})

	it('actions editors', () => {

	})

})

