/// <reference types="Cypress" />


import funct from '../support/functions'


const nameInput = '[data-test-id="new-tag-name-input"]'
const valueInput = '[data-test-id="new-tag-value-input"]'
const addButton = '[data-test-id="new-tag-add-button"]'
const drawerWindow = '.ant-drawer-wrapper-body'
const drawerTitle = '.ant-drawer-title'
const lagunaId = '@p:demo:r:1eeaf0ef-4d5f5c8f'
const textBox = '.ant-input'
const copyButton = '.anticon-copy'
const copiedCheckmark = '.anticon-check'



describe('Haystack Record Editor as Drawer',()=>{
    beforeEach(()=>{
        cy.visit('/iframe.html?id=haystack-record-editor--as-drawer')
        cy.window().then(win => {//disable Chrome alerts
            cy.stub(win, 'prompt').returns('DISABLED WINDOW PROMPT');
        });
    })
    it('The window and its components are displayed ',()=>{
      const randomTag = funct.lowerCaseRandomString(funct.getRandomIntBetweenMinAndMax(3,50))
      const randomMarkerTag = funct.randomString(funct.getRandomInt(50))
      const markerTagsSection = '.markers--3CJc0'

        cy.get(drawerWindow).should('be.visible')
          .find(drawerTitle).should('contain.text','Edit "Laguna" (@p:demo:r:1eeaf0ef-4d5f5c8f)');
        cy.get(drawerWindow).contains('id').should('be.visible')
          .parent().parent().contains(lagunaId).should('be.visible')
          .find(copyButton).should('be.visible').click()
          .parent().find(copiedCheckmark).should('be.visible');
        cy.get(drawerWindow).contains('dis').should('be.visible')
          .parent().parent().find(textBox).should('be.visible').should('have.value','Laguna');
        
        cy.get(markerTagsSection).children().contains('.ant-tag','Marker Tag').find('i').first().should('be.visible').click();
        cy.get(markerTagsSection).find('.ant-input').should('be.visible').type(randomMarkerTag).type('{enter}'); 
        cy.contains(randomMarkerTag).should('be.visible');
        cy.get(nameInput).type(randomTag).type('{enter}');
        cy.contains(randomTag).should('be.visible');
        //cy.get(markerTagsSection).find('.ant-input').type(randomTag).type('{enter}');
      
    })
})