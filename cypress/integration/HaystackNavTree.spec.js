/// <reference types="Cypress" />
/**
 * 
 *  file-name: HaystackNavTree.spec.js
 *  project-name: j2inn-ui
 * 
 *  Created by Quinatzin Sintora on 02/04/2020
 *  Copyright (c) 2020, J2 Innovation Inc.  All Rights Reserved
 * 
 * 
 */
describe("HaystackNavTree",()=>{
        
    //Log in and verify all Site before each 'it block'
    beforeEach(()=>{
        cy.visit('/iframe.html?id=haystack-nav-tree--basic');
        cy.get("[id='root']").should('exist').children("[role='tree']").should('exist');
        cy.get("[role='treeitem']").children('[title="City Center"]').should('be.visible');
        cy.get("[role='treeitem']").children('[title="JB Tower"]').should('be.visible');
        cy.get("[role='treeitem']").children('[title="Laguna"]').should('be.visible');
        cy.get("[role='treeitem']").children('[title="Malibu"]').should('be.visible');
        cy.get("[role='treeitem']").children('[title="Pearl Tower"]').should('be.visible');
    })

    it('Should show all Sites',()=>{
        cy.get("[id='root']").should('be.visible');
        cy.contains('City Center').should('be.visible');
        cy.contains('JB Tower').should('be.visible');
        cy.contains('Laguna').should('be.visible');
        cy.contains('Malibu').should('be.visible');
        cy.contains('Pearl Tower').should('be.visible');
    });

    it('City Center: should have Floors 1-4 an Utility check Equip',()=>{
        cy.contains('City Center').should('be.visible');
        cy.get("[role='treeitem']").children('[title="City Center"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();   
        for(var i = 1; i < 5; i++){ cy.contains("Floor " + i).should('be.visible'); }
        cy.contains('Utility').should('be.visible');

        //Open Floor 1 and check Equips
        //Close Floor 1 
        cy.get('[role="group"]').should('exist').children('[role="treeitem"]').should('exist');
        cy.get('[role="treeitem"]').children('[title="Floor 1"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('City Center AHU-1').should('be.visible');
        for(var i = 1; i < 10; i++){ cy.contains('City Center Vav-0' + i).should('be.visible'); }
        cy.contains('City Center Vav-10').should('be.visible');
        cy.get('[role="treeitem"]').children('[title="Floor 1"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('City Center AHU-1').should('not.be.visible');

        //Cpen Floor 2 
        //Verify City Center AHU-2 - VAV-11 - 20
        cy.get('[role="treeitem"]').children('[title="Floor 2"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('City Center AHU-2').should('be.visible');
        for(var i = 11; i < 21; i++){ cy.contains('City Center Vav-' + i).should('be.visible'); }
        cy.get('[role="treeitem"]').children('[title="Floor 2"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('City Center AHU-2').should('not.be.visible');

        //Open Floor 3
        //Verify City Center AHU-3 Vav-21 - 30
        //Close Floor 3
        cy.get('[role="treeitem"]').children('[title="Floor 3"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('City Center AHU-3').should('be.visible');
        for(var i = 21; i < 31; i++){ cy.contains('City Center Vav-' + i).should('be.visible'); }
        cy.get('[role="treeitem"]').children('[title="Floor 3"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('City Center AHU-3').should('not.be.visible');

        //Open Floor 4
        //Verify City Center AHU-4, Vav-31 - 40 
        //Close Floor 4
        cy.get('[role="treeitem"]').children('[title="Floor 4"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('City Center AHU-4').should('be.visible');
        for(var i = 31; i < 41; i++){ cy.contains('City Center Vav-' + i).should('be.visible'); }
        cy.get('[role="treeitem"]').children('[title="Floor 4"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('City Center AHU-4').should('not.be.visible');

        //Open Utility and Verify Equip City Center ElecMeter and Misc
        cy.get('[role="treeitem"]').children('[title="Utility "]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('City Center ElecMeter').should('be.visible');
        cy.contains('City Center Misc').should('be.visible');
        cy.get('[role="treeitem"]').children('[title="Utility "]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('City Center ElecMeter').should('not.be.visible');
    })

    it('JB Tower: should have Floor 1-2 and Utility check Equips',()=>{
        cy.contains('JB Tower').should('be.visible');
        cy.get("[role='treeitem']").children('[title="JB Tower"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();   
        for(var i = 1; i <= 2; i++){ cy.contains("Floor " + i).should('be.visible'); }
        cy.contains('Utility').should('be.visible');

        //Open Floor 1 and check Equips
        //Close Floor 1 
        cy.get('[role="group"]').should('exist').children('[role="treeitem"]').should('exist');
        cy.get('[role="treeitem"]').children('[title="Floor 1"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('JB Tower AHU-1').should('be.visible');
        for(var i = 1; i <= 9; i++){ cy.contains('JB Tower Vav-0' + i).should('be.visible'); }
        cy.contains('JB Tower Vav-10').should('be.visible');
        cy.get('[role="treeitem"]').children('[title="Floor 1"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('JB Tower AHU-1').should('not.be.visible');

        //Open Floor 2 and Check Equips
        cy.get('[role="group"]').should('exist').children('[role="treeitem"]').should('exist');
        cy.get('[role="treeitem"]').children('[title="Floor 2"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('JB Tower AHU-2').should('be.visible');
        for(var i = 11; i <= 20; i++){ cy.contains('JB Tower Vav-' + i).should('be.visible'); }
        cy.get('[role="treeitem"]').children('[title="Floor 2"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('JB Tower AHU-2').should('not.be.visible');

        //Open Utility and check Equips
        cy.get('[role="group"]').should('exist').children('[role="treeitem"]').should('exist');
        cy.get('[role="treeitem"]').children('[title="Utility "]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('JB Tower ElecMeter').should('be.visible');
        cy.contains('JB Tower Misc').should('be.visible');
        cy.get('[role="treeitem"]').children('[title="Utility "]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('JB Tower ElecMeter').should('not.be.visible');
    })

    it('Laguna: should have Floor 1 and Utility check Equips',()=>{
        cy.contains('Laguna').should('be.visible');
        cy.get("[role='treeitem']").children('[title="Laguna"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();   
        cy.contains('Floor 1').should('be.visible');
        cy.contains('Utility').should('be.visible');

        //Open Floor 1 and check Equip
        cy.get('[role="group"]').should('exist').children('[role="treeitem"]').should('exist');
        cy.get('[role="treeitem"]').children('[title="Floor 1"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        for(var i = 1; i <= 5; i++){ cy.contains('Laguna RTU-' + i).should('be.visible'); }
        cy.get('[role="treeitem"]').children('[title="Floor 1"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('Laguna RTU-5').should('not.be.visible');
    
        //Open Utility and check Equips
        cy.get('[role="treeitem"]').children('[title="Utility "]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('Laguna ElecMeter').should('be.visible');
        cy.contains('Laguna Misc').should('be.visible');
        cy.get('[role="treeitem"]').children('[title="Utility "]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
    })

    it('Malibu: should have Floor 1 and Utility check Equips',()=>{
        cy.contains('Malibu').should('be.visible');
        cy.get("[role='treeitem']").children('[title="Malibu"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();   
        cy.contains('Floor 1').should('be.visible');
        cy.contains('Utility').should('be.visible');

        //Open Floor 1 and check Equip
        //Close Floor 1
        cy.get('[role="treeitem"]').children('[title="Floor 1"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        for(var i = 1; i <=5; i++){ cy.contains('Malibu RTU-' + i).should('be.visible');}
        cy.get('[role="treeitem"]').children('[title="Floor 1"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
    
        //Open Utility and check Equips
        cy.get('[role="treeitem"]').children('[title="Utility "]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('Malibu ElecMeter').should('be.visible');
        cy.contains('Malibu Misc').should('be.visible');
        cy.get('[role="treeitem"]').children('[title="Utility "]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
    })

    it('Pearl Tower: should have Center Plant and Utility check Equips',()=>{
        cy.contains('Pearl Tower').should('be.visible');
        cy.get("[role='treeitem']").children('[title="Pearl Tower"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();   
        cy.contains('Central Plant').should('be.visible');
        cy.contains('Utility').should('be.visible');

        //Open Central Plant and check Equips
        //Close Central Plant
        cy.get('[role="treeitem"]').children('[title="Central Plant"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('Pearl Tower Boiler').should('be.visible');
        cy.contains('Pearl Tower Chiller').should('be.visible');
        cy.get('[role="treeitem"]').children('[title="Central Plant"]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();

        //Open Utility and check Equips
        //Close Utility
        cy.get('[role="treeitem"]').children('[title="Utility "]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
        cy.contains('Pearl Tower ElecMeter').should('be.visible');
        cy.contains('Pearl Tower Misc').should('be.visible');
        cy.get('[role="treeitem"]').children('[title="Utility "]').should('be.visible').siblings('.ant-tree-switcher').should('be.visible').click();
    })
})