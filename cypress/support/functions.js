export  default{
  randomString(len) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for ( var i = 0; i < len; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return result;
    },

    getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max)) + 1;
  },
  lowerCaseRandomString(len) {
    var result           = '';
    var letters = 'abcdefghijklmnopqrstuvwxyz';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    result += letters.charAt(Math.floor(Math.random() * letters.length));
    for ( var i = 0; i < len; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return result;
  } ,
  getRandomIntBetweenMinAndMax(min,max){
    return (Math.floor(Math.random() * (max - min + 1) ) + min);
  } , 
  getRadonLetter(){
      var letters =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
      return letters.charAt(Math.floor(Math.random() * letters.length));
  },
  randomPositiveOrNegative(){
      var posNeg = '- ';
      return posNeg.charAt(Math.floor(Math.random() * posNeg.length));
  } 

}
