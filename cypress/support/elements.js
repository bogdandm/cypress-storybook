const id = ident => `[id="${ident}"]`

// elements
// Rules:
//  data-test-ids should be lower-case-hyphen-seperated (similiar to css classes)
//  but the key in elements should be lowerCamelCase, except when referring to a specific
//  named React component, e.g. AppLauncher, RelatedGraphics, etc
export default {
    
    mainRoot: id`root`,

    //Haystack Record Selector
    searchBox: `[class*="ant-input-search"]`,
    clearSearchBoxButton:  `i[aria-label="icon: close-circle"]`,
    recordTree: `[role="tree"]`,
    recordTreeLine:`[role="treeitem"]`,
    expandCollapseButton: `[class*="ant-tree-switcher"]`,

    //Search Result
    searchResultTable: `[class*="ant-table-tbody"]`,
    searchResultRow: `[class*="ant-table-row"]`,
    resultInfo: `[class*="ant-popover-inner-content"]`,
    
    //pagination
    pagination: `[class*="ant-pagination"]`
}