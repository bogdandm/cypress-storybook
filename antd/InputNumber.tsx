import React from 'react'
import {InputNumber as AntInputNumber} from 'antd'
import {InputNumberProps} from 'antd/lib/input-number'

import 'antd/es/input-number/style/index.less'

export const InputNumber = React.forwardRef((props: InputNumberProps, ref) => {
	const {min = 0, step = 1, ...addl} = props
	return <AntInputNumber ref={ref} min={min} step={step} {...addl} />
})

InputNumber.displayName = 'InputNumber'
