import React from 'react'
import {DatePicker as AntDatePicker} from 'antd'
import {DatePickerProps} from 'antd/lib/date-picker/interface'

import 'antd/es/date-picker/style/index.less'

const format = 'YYYY-MM-DD'
const formatWithTime = 'YYYY-MM-DD HH:mm'

export class DatePicker extends React.Component<DatePickerProps> {

	render() {
		const props = this.props
		return (
			<AntDatePicker
				format={props.showTime ? formatWithTime : format}
				allowClear={false}
				style={{minWidth: 195, width: ''}}
				{...props}
			/>
		)
	}

	static RangePicker = AntDatePicker.RangePicker
	static MonthPicker = AntDatePicker.MonthPicker
	static WeekPicker = AntDatePicker.WeekPicker

}

