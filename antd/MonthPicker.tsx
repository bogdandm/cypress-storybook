import React from 'react'
import {DatePicker} from 'antd'
import {MonthPickerProps as AntMonthPickerProps} from 'antd/lib/date-picker/interface'

import 'antd/es/date-picker/style/index.less'

const format = 'YYYY MMM'
const fullFormat = 'YYYY MMMM'

type MonthPickerProps = {
	full: undefined | null | boolean
}

export function MonthPicker(props: MonthPickerProps & AntMonthPickerProps) {
	return (
		<DatePicker.MonthPicker
			format={props.full ? fullFormat : format}
			allowClear={false}
			placeholder='Select month'
			style={props.full ? null : {width: 110}}
			{...props}
		/>
	)
}
