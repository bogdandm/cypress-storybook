import React from 'react'
import {Button as AntButton} from 'antd'
import {ButtonProps} from 'antd/lib/button'

import 'antd/es/button/style/index.less'

function Button(props: ButtonProps) {
	return <AntButton {...props} />
}

Button.Group = AntButton.Group

export {Button}
