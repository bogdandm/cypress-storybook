import React from 'react'
import {Form as AntForm} from 'antd'
import {FormProps, FormItemProps, FormCreateOption} from 'antd/lib/form'

import './Form.global.styl'
import 'antd/es/form/style/index.less'

type CreateOptions = {
	storeModelProp: undefined | null | string
}

export class Form extends React.Component<FormProps> {

	render() {
		return <AntForm {...this.props} />
	}

	static Item = function FormItem(props: FormItemProps) {
		return <AntForm.Item required={false} colon={false} {...props} />
	}

	static create = function(args: CreateOptions & FormCreateOption<{store: unknown}>) {
		const {storeModelProp, ...options} = args || {}
		// this "connects" the AntD form with a model on the store
		// For example, PlaneFormStore has a `plane` property that is a Plane model...
		if (storeModelProp) {
			// This copies over values of the Form field values to matching properties of PlaneFormStore.plane
			options.onFieldsChange = function(props, changedField) {
				if (props.store && props.store[storeModelProp]) {
					const modelInstance = props.store[storeModelProp]
					const proto = Object.getPrototypeOf(modelInstance)
					Object.keys(changedField).forEach(field => {
						if (proto.hasOwnProperty(field)) {
							modelInstance[field] = changedField[field].value
						}
					})
				}
			}
		}
		return AntForm.create(options)
	}
}

