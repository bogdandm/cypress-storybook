import React from 'react'
import moment from 'moment'
import {DatePicker} from 'antd'
import {RangePickerProps} from 'antd/lib/date-picker/interface'

const AntRangePicker = DatePicker.RangePicker

const defaultRanges = {
	'Today': [moment().startOf('day'), moment().endOf('day')],
	'Yesterday': [
		moment()
			.subtract(1, 'day')
			.startOf('day'),
		moment()
			.subtract(1, 'day')
			.endOf('day'),
	],
	'This Week': [moment().startOf('week'), moment().endOf('week')],
	'Last Week': [
		moment()
			.subtract(1, 'week')
			.startOf('week'),
		moment()
			.subtract(1, 'week')
			.endOf('week'),
	],
	'This Month': [moment().startOf('month'), moment().endOf('month')],
	'Next Month': [
		moment()
			.add(1, 'month')
			.startOf('month'),
		moment()
			.add(1, 'month')
			.endOf('month'),
	],
}

export function RangePicker(props: RangePickerProps) {
	return <AntRangePicker ranges={defaultRanges} {...props} />
}
