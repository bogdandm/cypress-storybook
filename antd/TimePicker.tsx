import React from 'react'
import {TimePicker as AntTimePicker} from 'antd'
import {TimePickerProps} from 'antd/lib/time-picker'

import 'antd/es/time-picker/style/index.less'

function reparent(node: HTMLElement): HTMLElement {
	return node.parentNode
}
export class TimePicker extends React.Component<TimePickerProps> {

	render() {
		const props = this.props
		return (
			<AntTimePicker
				getPopupContainer={reparent}
				format='HH:mm'
				allowClear={false}
				{...props}
			/>
		)
	}

}
