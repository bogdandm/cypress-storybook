import React from 'react'
import {Popover as AntPopover} from 'antd'
import {observable} from 'mobx'
import {observer} from 'mobx-react'

import 'antd/es/popover/style/index.less'

@observer
export class Popover extends React.Component {

	@observable remainVisible = false

	constructor(props) {
		super(props)

		this.clickHandler = this.clickHandler.bind(this)
	}

	onVisibleChange = () => {
		if (this.remainVisible) {
			this.remainVisible = false
		}
	}

	clickHandler = event => {
		if (!this.popover) {
			return
		}

		//getPopupContainer
		let container = this.popover.getPopupDomNode()

		if (!container) {
			return
		}

		container = container.parentNode.parentNode

		if (container.contains(event.target)) {
			this.remainVisible = true
		}
	}

	componentDidMount() {
		document.addEventListener('click', this.clickHandler)
	}

	componentWillUnmount() {
		document.removeEventListener('click', this.clickHandler)
	}

	render() {
		let addl = {}
		if (this.remainVisible) {
			addl.visible = true
		}
		return (
			<AntPopover
				ref={ref => (this.popover = ref)}
				onVisibleChange={this.onVisibleChange}
				autoAdjustOverflow
				arrowPointAtCenter
				{...this.props}
				{...addl}>
				{this.props.children}
			</AntPopover>
		)
	}

}
