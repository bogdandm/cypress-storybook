import React from 'react'
import {Input as AntInput} from 'antd'
import {InputProps} from 'antd/lib/input'

import 'antd/es/input/style/index.less'

export const Input = React.forwardRef((props: InputProps, ref) => {
	let {name, placeholder, ...addl} = props
	if (name && !placeholder) {
		placeholder = name
	}
	return (
		<AntInput ref={ref} name={name} placeholder={placeholder} {...addl} />
	)
})

Input.Search = AntInput.Search
Input.TextArea = AntInput.TextArea
Input.Group = AntInput.Group
Input.displayName = 'Input'
