import React, {useState} from 'react'
import {Drawer as AntDrawer} from 'antd'
import {DrawerProps as AntDrawerProps} from 'antd/lib/drawer'
import 'antd/es/drawer/style/index.less'
import classnames from 'classnames'

import classes from './Drawer.styl'

type DrawerProps = {
  trigger: undefined | null | JSX.Element
  children: undefined | null | JSX.Element
}

function ControlledDrawer(props: DrawerProps & AntDrawerProps) {
	const {trigger, children, ...rest} = props
	const [isOpen, setOpen] = useState(!trigger)
	return (
		<>
			{trigger && <div onClick={() => setOpen(true)}>{trigger}</div>}
			<AntDrawer
				className={classnames(classes.modal, props.className)}
				visible={isOpen}
				{...rest}
				onClose={e => {
					props.onClose && props.onClose(e)
					setOpen(false)
				}}>
				{children}
			</AntDrawer>
		</>
	)
}

export function Drawer(props) {
	return <ControlledDrawer {...props} />
}
