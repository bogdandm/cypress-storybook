import React, {useState} from 'react'
import {Modal as AntModal} from 'antd'
import {ModalProps as AntModalProps} from 'antd/lib/modal'
import 'antd/es/modal/style/index.less'
import classnames from 'classnames'

import style from './Modal.styl'

type ModalProps = {
	trigger: undefined | null | JSX.Element
	children: undefined | null | JSX.Element
}

function ControlledModal(props: ModalProps & AntModalProps) {
	const {trigger, children, ...rest} = props
	const [isOpen, setOpen] = useState(!trigger)
	return (
		<>
			{trigger && <div onClick={() => setOpen(true)}>{trigger}</div>}
			<AntModal
				visible={isOpen}
				{...props}
				className={classnames(style.modal, props.className)}
				onCancel={e => {
					props.onCancel && props.onCancel(e)
					setOpen(false)
				}}
				onOk={e => {
					props.onOk && props.onOk(e)
					setOpen(false)
				}}>
				{children}
			</AntModal>
		</>
	)
}

class Modal extends React.Component {

	render() {
		const {form, ...props} = this.props
		if (form) {
			const dirty = form.isFieldsTouched()
			if (!dirty) {
				props.cancelButtonProps = {
					...props.cancelButtonProps,
					style: {display: 'none'},
				}
				props.okButtonProps = {
					disabled: true,
				}
			}
		}
		return <ControlledModal {...props} />
	}

}

Modal.info = AntModal.info
Modal.success = AntModal.success
Modal.error = AntModal.error
Modal.warning = AntModal.warning
Modal.confirm = AntModal.confirm

export {Modal}
