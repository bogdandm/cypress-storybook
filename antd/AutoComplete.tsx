import React from 'react'
import {AutoComplete as AntAutoComplete} from 'antd'
import {AutoCompleteProps} from 'antd/lib/auto-complete'

import 'antd/es/auto-complete/style/index.less'

export function AutoComplete(props: AutoCompleteProps) {
	return (
		<AntAutoComplete
			backfill
			filterOption={false}
			allowClear={false}
			defaultActiveFirstOption={false}
			showArrow={false}
			{...props}
		/>
	)
}

AutoComplete.Option = AntAutoComplete.Option
AutoComplete.OptGroup = AntAutoComplete.OptGroup
