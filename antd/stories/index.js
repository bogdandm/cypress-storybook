import './css/style.css'
import './css/icons.css'

require('./General/')
require('./Navigation/')
require('./Data Entry/')
require('./Data Display/')
require('./Feedback/')
require('./Other/')
