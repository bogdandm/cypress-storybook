import React from 'react'
import {Drawer, Button} from '../../../../antd'

export default class App extends React.Component {

	render() {
		return (
			<div>
				<Drawer
					title='Controlled Drawer'
					trigger={
						<Button type='primary' onClick={this.showDrawer}>
							Open
						</Button>
					}>
					<p>Some contents...</p>
					<p>Some contents...</p>
					<p>Some contents...</p>
				</Drawer>
			</div>
		)
	}

}
