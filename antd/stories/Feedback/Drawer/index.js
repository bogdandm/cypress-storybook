import React, {Component, Fragment} from 'react'

import Basic from './Basic'
import Controlled from './Controlled'
import FormInDrawer from './FormInDrawer'
import Multilevel from './Multilevel'
import Position from './Position'
import UserProfile from './UserProfile'

export default class DrawerPage extends Component {

	render() {
		return (
			<Fragment>
				<h1 className='title'>Drawer</h1>

				<p className='text'>
					A panel which slides in from the edge of the screen
				</p>

				<h2 className='title'>When To Use</h2>

				<p className='text'>
					A Drawer is a panel that is typically overlaid on top of a
					page and slides in from the side. It contains a set of
					information or actions. Since the user can interact with the
					Drawer without leaving the current page, tasks can be
					achieved more efficiently within the same context.
				</p>

				<h2 className='title'>Examples:</h2>

				<section className='example'>
					<h3 className='ex-title'>Basic</h3>
					<Basic />
				</section>
				<section className='example'>
					<h3 className='ex-title'>Stateless w/ trigger</h3>
					<p>
						No need to control <code>visible</code> prop when using{' '}
						<code>trigger</code>
					</p>
					<Controlled />
				</section>
				<section className='example'>
					<h3 className='ex-title'>Form In Drawer</h3>
					<FormInDrawer />
				</section>
				<section className='example'>
					<h3 className='ex-title'>Multilevel</h3>
					<Multilevel />
				</section>
				<section className='example'>
					<h3 className='ex-title'>Position</h3>
					<Position />
				</section>
				<section className='example'>
					<h3 className='ex-title'>User Profile</h3>
					<UserProfile />
				</section>
			</Fragment>
		)
	}

}
