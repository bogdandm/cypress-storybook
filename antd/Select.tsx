import React from 'react'
import {Select as AntSelect} from 'antd'

import 'antd/es/select/style/index.less'

function getRootNode(node) {
	return node
}

class Select extends React.Component {

	render() {
		return <AntSelect getPopupContainer={getRootNode} {...this.props} />
	}

	static Option = AntSelect.Option
	static OptGroup = AntSelect.OptGroup

}

export {Select}
