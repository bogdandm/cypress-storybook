#!/usr/bin/env bash

set -ex

# Build, just in case
./build.sh

# Cp our package.json over so "dist" folder becomes the package root
cp package.json dist

# Publish the dist folder, not the root
npm publish dist
