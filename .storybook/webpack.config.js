const path = require('path')
const fs = require('fs')
const webpack = require('webpack')
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin')

const loaders = require('@j2inn/react-config/webpack.loaders.js')

class CleanUpStatsPlugin {
	shouldPickStatChild(child) {
		return child.name.indexOf('extract-css-chunks-webpack-plugin') !== 0
	}

	apply(compiler) {
		compiler.plugin('done', stats => {
			if (Array.isArray(stats.compilation.children)) {
				stats.compilation.children = stats.compilation.children.filter(
					child => this.shouldPickStatChild(child)
				)
			}
		})
	}
}

// Export a function. Accept the base config as the only param.
module.exports = async ({config, mode}) => {
	// `mode` has a value of 'DEVELOPMENT' or 'PRODUCTION'
	// You can change the configuration based on that.
	// 'PRODUCTION' is used when building the static version of storybook.

	const DEBUG = mode === 'DEVELOPMENT'
	const PRODUCTION = !DEBUG

	config.devtool = 'inline-module-source-map'

	config.module.rules = loaders

	config.module.rules.push({
		test: /stories\.js$/,
		loaders: [
			{
				loader: require.resolve('@storybook/addon-storysource/loader'),
				options: {
					prettierConfig: {
						trailingComma: 'es5',
						tabWidth: 4,
						semi: false,
						singleQuote: true,
						useTabs: true,
						bracketSpacing: true,
						jsxBracketSameLine: true,
						jsxSingleQuote: true,
					},
				},
			},
		],
		enforce: 'pre',
	})

	config.module.rules.push({
		test: /\.(ts|tsx)$/,
		loader: require.resolve('babel-loader'),
		options: {
			presets: [['react-app', { flow: false, typescript: true }]],
		},
	})

	config.resolve.extensions.push('.ts', '.tsx')

	config.plugins.push(new CleanUpStatsPlugin())

	config.plugins.push(
		new ExtractCssChunks({
			hot: true,
			orderWarning: true,
			reloadAll: true,
			cssModules: true,
		})
	)

	config.plugins.push(
		new webpack.DefinePlugin({
			DEBUG,
			PRODUCTION,
		})
	)

	return config
}
