import path from 'path'
import {configure, addParameters} from '@storybook/react'
import {INITIAL_VIEWPORTS} from '@storybook/addon-viewport'

// automatically import all files ending in *.stories.js
const comps = require.context('../components', true, /stories\.js$/)
const antd = require.context('../antd', true, /(stories\.js)$/)

function loadStories() {
	comps.keys().forEach(filename => comps(filename))
	antd.keys().forEach(filename => antd(filename))

	require('../antd/stories/')
}

addParameters({viewport: {viewports: INITIAL_VIEWPORTS}})
configure(loadStories, module)
