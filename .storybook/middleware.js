const proxy = require('http-proxy-middleware')

const finstackURL = `http://${process.env.FINSTACK_HOST || 'localhost:8080'}`

const finProxyUrls = {
	'/auth/*': finstackURL,
	'/api/*': finstackURL,
	'/fin5Lang/*': finstackURL,
	'/finStackAuth': finstackURL,
	'/finWebApp/finstack': finstackURL,
	'/finGetFile/*': finstackURL,
	'/user/login': finstackURL,
}

module.exports = function expressMiddleware(router) {
	for (let domain in finProxyUrls) {
		if (typeof finProxyUrls[domain] === 'string') {
			router.use(
				domain,
				proxy({
					target: finProxyUrls[domain],
				})
			)
		} else {
			router.use(domain, proxy(finProxyUrls[domain]))
		}
	}
}
