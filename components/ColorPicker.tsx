import React from 'react'
import {ChromePicker} from 'react-color'

const styles = {
	default: {
		picker: {
			boxShadow: 0,
		},
	},
}

export function ColorPicker({value, onChange, ...props}) {
	return (
		<ChromePicker
			{...props}
			color={value}
			onChangeComplete={onChange}
			styles={styles}
		/>
	)
}
