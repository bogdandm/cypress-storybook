import React, {useState, useEffect, DOMElement, HTMLAttributes} from 'react'
import {usei18n} from '@j2inn/utils'
import {Icon, Button, Tooltip, Input} from '../../antd'
import {Container, Spacer} from '../Container'
import {HaystackGridTable} from '../HaystackGridTable'
import {NavTree} from '../NavTree'
import classnames from 'classnames'

import classes from './styles.styl'

const query = async (q, cb) => {
	if (q && q.length > 0) {
		const {result} = await finstack.eval(q)
		cb(result)
	} else {
		cb(null)
	}
}

interface HaystackRecordSelectorProps {
	value?: any;
	treeName?: string;
	onSelect?: Function; // TODO better type
	onSelectionChange?: Function; // TODO better type
	multiple?: boolean;
}

export function HaystackRecordSelector({
	value,
	onSelect,
	treeName,
	multiple,
	...rest
}: HaystackRecordSelectorProps) {
	const [axonquery, setAxonquery] = useState()
	const [grid, setGrid] = useState()
	const [useHaystackFilter, setUseHaystackFilter] = useState(true)
	const [selection, setSelection] = useState()
	const i18n = usei18n()

	useEffect(() => {
		query(axonquery, setGrid)
	}, [axonquery])

	const hasResults = grid && grid.gridRows.length > 0

	return (
		<div {...rest}>
			<Container horizontal nowrap style={{width: '100%'}}>
				<Input.Search
					prefix={
						<Tooltip
							title={
								useHaystackFilter
									? i18n.get('Search by string. Click to search with a Haystack filter')
									: i18n.get('Search with a Haystack filter. Click to search by string.')
							}>
							<Icon
								type={useHaystackFilter ? 'tag' : 'font-size'}
								onClick={() =>
									setUseHaystackFilter(!useHaystackFilter)
								}
							/>
						</Tooltip>
					}
					placeholder={`Search ${
						useHaystackFilter
							? i18n.get('with a Haystack filter')
							: i18n.get('for a string')
					}...`}
					onChange={(e: InputEvent) => setAxonquery(e.target.value)}
					allowClear
					style={{flex: 1}}
				/>
			</Container>
			<Container
				center
				middle
				vertical
				nowrap
				className={classnames(classes.resultsTable, {
					[classes.hasResults]: hasResults,
				})}>
				{hasResults ? (
					<HaystackGridTable
						grid={grid}
						showColumns={['dis']}
						showHeader={false}
						size='small'
						onRow={record => {
							return {
								class:
									selection === record.id
										? classes.selectedRow
										: null,
								onClick: () => {
									setSelection(record.id)
									onSelect &&
										onSelect(
											multiple ? [record.id] : record.id
										)
								},
							}
						}}
					/>
				) : axonquery ? (
					i18n.get('No results')
				) : (
					<NavTree
						value={value}
						onSelect={keys =>
							onSelect && onSelect(multiple ? keys : keys[0])
						}
						treeName={treeName}
						multiple={multiple}
						selectedKeys={[value]}
					/>
				)}
			</Container>
		</div>
	)
}
