import React from 'react'
import {storiesOf} from '@storybook/react'
import {withKnobs, select, text} from '@storybook/addon-knobs'
import {action} from '@storybook/addon-actions'
import {unescape} from 'lodash'

import {HaystackRecordSelector} from './index'

storiesOf('Haystack Record Selector', module)
	.addDecorator(withKnobs)
	.add('basic', () => {
		const props = {
			onChange: action('onChange')
		}
		return <div style={{padding: '1em'}}>
			<HaystackRecordSelector {...props}/>
		</div>
	})
