import React, { DOMElement, HTMLAttributes, DOMAttributes, StyleHTMLAttributes, CSSProperties } from 'react'

const autoFit = 'repeat(auto-fit, minmax(100px, 1fr))'

type ContainerProps = {
	// display
	inline?: boolean

	//grid
	grid?: boolean | string
	columns?: string
	cols?: string
	gap?: string
	evenly?: boolean
	columnWidth?: number
	colWidth?: number

	// Flexbox
	// left/right (x axis)
	horizontal?: boolean
	left?: boolean
	center?: boolean
	right?: boolean

	// up/down (y axis)
	vertical?: boolean
	top?: boolean
	bottom?: boolean
	middle?: boolean

	// modifiers
	loose?: boolean
	around?: boolean
	between?: boolean
	tight?: boolean
	stretch?: boolean
	wrapreverse?: boolean
	nowrap?: boolean

	class?: string
	className?: string

	style?: CSSProperties

}

export function Container(props: ContainerProps & DOMAttributes<{}>): JSX.Element {

	const {
		// display
		inline,

		//grid
		grid,
		columns = props.cols,
		gap,
		evenly,
		colWidth,

		// Flexbox
		// left/right (x axis)
		horizontal = true,
		left,
		center,
		right,

		// up/down (y axis)
		vertical,
		top,
		bottom,
		middle,

		// modifiers
		loose,
		around,
		between,
		tight,
		stretch,
		wrapreverse,
		nowrap,

		children,
		style,

		...rest

	} = props

	// styles
	let s: CSSProperties = {}

	if (grid) {
		s.display = inline ? 'inline-grid' : 'grid'
		s.grid = columns
			? `auto / repeat(${columns}, 1fr)`
			: colWidth
				? `auto / repeat(auto-fit, ${
					typeof colWidth === 'number'
						? colWidth + 'px'
						: colWidth
			  })`
				: grid === true
					? vertical
						? `${autoFit} / auto`
						: `auto / ${autoFit}`
					: grid
		s.gap = gap ? gap : '1rem'
		s.justifyContent = left
			? 'flex-start'
			: center
				? 'center'
				: right
					? 'flex-end'
					: around
						? 'space-around'
						: between
							? 'space-between'
							: stretch
								? 'stretch'
								: evenly
									? 'space-evenly'
									: colWidth
										? 'space-between' // nice default to have if nothing else specified
										: null

		s.gridAutoFlow = 'dense'

		s.alignContent = top
			? 'flex-start'
			: middle
				? 'center'
				: bottom
					? 'flex-end'
					: around
						? 'space-around'
						: between
							? 'space-between'
							: evenly
								? 'space-evenly'
								: 'flex-start'
	} else {
		s.display = inline ? 'inline-flex' : 'flex'
		s.flexWrap = nowrap ? 'nowrap' : wrapreverse ? 'wrap-reverse' : 'wrap'

		let x = left ? 'flex-start' : right ? 'flex-end' : 'center'
		let y = top ? 'flex-start' : bottom ? 'flex-end' : 'center'

		if (vertical) {
			s.flexDirection = 'column'
			s.justifyContent = evenly
				? 'space-evenly'
				: around
					? 'space-around'
					: between
						? 'space-between'
						: y
			s.alignContent = loose ? null : y
			s.alignItems = stretch ? null : x
		} else if (horizontal) {
			// horizontal (default)
			s.flexDirection = 'row'

			s.justifyContent = evenly
				? 'space-evenly'
				: around
					? 'space-around'
					: between
						? 'space-between'
						: x
			s.alignContent = loose ? null : y
			s.alignItems = stretch ? null : y
		}
	}

	return <div style={{...s, ...style}} className={props.class || props.className} {...rest}>
		{children}
	</div>
}

interface SpacerProps {
	grow?: number
	shrink?: number
	basis?: string
	style?: CSSProperties
}

export function Spacer(props: SpacerProps) {
	const {
		grow = 1,
		shrink = 1,
		basis = 'auto',
		style,
		...rest
	} = props
	return (
		<span
			style={{flex: `${grow} ${shrink} ${basis}`, ...style}}
			{...rest}
		/>
	)
}
