import React from 'react'
import hoc from 'react-loadable'

const defaultLoading = () => <div className='loading' />

export function Loadable(loader, loading = defaultLoading): Loadable {
	return hoc({loader, loading})
}
