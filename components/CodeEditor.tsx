import React, {useState, useEffect, useRef} from 'react'
import {isFunction} from 'lodash'
import {Modal} from '../antd'
import 'ace-builds'
import AceEditor from 'react-ace'
import 'ace-builds/webpack-resolver'

import 'ace-builds/src-min-noconflict/ext-searchbox'
import 'ace-builds/src-min-noconflict/mode-javascript'
import 'ace-builds/src-min-noconflict/theme-solarized_dark'

import prettier from 'prettier/standalone'

const parsers = {
	html: () => import('prettier/parser-html'),
	css: () => import('prettier/parser-postcss'),
	typescript: () => import('prettier/parser-typescript'),
	json: () => import('prettier/parser-babylon'),
	babylon: () => import('prettier/parser-babylon'),
}

Object.keys(parsers).forEach(lang => {
	const loader = parsers[lang]
	parsers[lang] = () => loader().then(mod => parsers[lang] = mod.default)
})

export const modes = {
	java: () => import('ace-builds/src-min-noconflict/mode-java'),
	python: () => import('ace-builds/src-min-noconflict/mode-python'),
	xml: () => import('ace-builds/src-min-noconflict/mode-xml'),
	ruby: () => import('ace-builds/src-min-noconflict/mode-ruby'),
	sass: () => import('ace-builds/src-min-noconflict/mode-sass'),
	markdown: () => import('ace-builds/src-min-noconflict/mode-markdown'),
	mysql: () => import('ace-builds/src-min-noconflict/mode-mysql'),
	json: () => import('ace-builds/src-min-noconflict/mode-json'),
	html: () => import('ace-builds/src-min-noconflict/mode-html'),
	handlebars: () => import('ace-builds/src-min-noconflict/mode-handlebars'),
	golang: () => import('ace-builds/src-min-noconflict/mode-golang'),
	csharp: () => import('ace-builds/src-min-noconflict/mode-csharp'),
	coffee: () => import('ace-builds/src-min-noconflict/mode-coffee'),
	css: () => import('ace-builds/src-min-noconflict/mode-css'),
}

export const themes = {
	github: () => import('ace-builds/src-min-noconflict/theme-github'),
	tomorrow: () => import('ace-builds/src-min-noconflict/theme-tomorrow'),
	kuroir: () => import('ace-builds/src-min-noconflict/theme-kuroir'),
	twilight: () => import('ace-builds/src-min-noconflict/theme-twilight'),
	xcode: () => import('ace-builds/src-min-noconflict/theme-xcode'),
	textmate: () => import('ace-builds/src-min-noconflict/theme-textmate'),
	monokai: () => import('ace-builds/src-min-noconflict/theme-monokai'),
	terminal: () => import('ace-builds/src-min-noconflict/theme-terminal'),
}

const defaultOptions = {
	trailingComma: 'es5',
	tabWidth: 4,
	useTabs: true,
	singleQuote: true,
	bracketSpacing: true,
	semi: false,
	htmlWhitespaceSensitivity: 'ignore'
}

async function format(code, mode) {
	let parser = 'babylon', options = {...defaultOptions}
	if(mode === 'html' || mode === 'xml') {
		parser = 'html'
	}
	if(mode === 'css') {
		parser = 'css'
	}
	if(mode === 'typescript') {
		parser = 'typescript'
	}
	if(mode === 'json' || mode === 'json5') {
		parser = 'json'
	}
	if(mode === 'yaml') {
		parser = 'yaml'
	}
	if(isFunction(parsers[parser])) {
		await parsers[parser]()
	}
	try {
		return prettier.format(code, {...options, parser, plugins: [parsers[parser]]})
	} catch(e) {
		Modal.error({
			title: 'Formatting Error',
			width: '80vw',
			content: (
				<>
					An error occurred formatting the code:
					<pre style={{background: '#f0f0f0', padding: '1em', maxHeight: 300}}>{e.message}</pre>
				</>
			),
			centered: true,
		})
	}
}

export function CodeEditor(props) {
	let {mode, theme, editorProps, value, beautify = false, ...rest} = props

	const [currentTheme, setTheme] = useState('solarized_dark')
	const [currentMode, setMode] = useState('javascript')

	const ref = useRef()

	useEffect(() => {
		(async function() {
			const loader = themes[theme]
			if (loader && typeof loader === 'function') {
				themes[theme] = loader()
				await themes[theme]
			}
			setTheme(theme)
		})()
	}, [props.theme])

	useEffect(() => {
		(async function() {
			const loader = modes[mode]
			if (loader && typeof loader === 'function') {
				modes[mode] = loader()
				await modes[mode]
			}
			setMode(mode)
		})()
	}, [props.mode])

	useEffect(() => {
		(async function() {
			if(beautify && ref && ref.current) {
				const {editor} = ref.current
				const formatted = await format(value, mode || currentMode)
				editor.setValue(formatted)
			}
		})()
	}, [beautify, value])

	const commands = [
		{
			name: 'format', //name for the key binding.
			bindKey: {win: 'Ctrl-Shift-f', mac: 'Command-Shift-f'}, //key combination used for the command.
			exec: async () => {
				if(ref && ref.current) {
					const {editor} = ref.current
					const formatted = await format(editor.getValue(), mode || currentMode)
					editor.setValue(formatted)
				}
			}  //function to execute when keys are pressed.
		}
	]

	return (
		<AceEditor
			value={value}
			mode={currentMode}
			theme={currentTheme}
			name='test'
			editorProps={{$blockScrolling: true, ...editorProps}}
			commands={commands}
			ref={ref}
			{...rest}
		/>
	)
}
