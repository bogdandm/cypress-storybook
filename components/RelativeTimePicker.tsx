import React from 'react'
import {observer} from 'mobx-react'
import {observable, computed} from 'mobx'
import {usei18n} from '@j2inn/utils'
import {Icon, Dropdown, Menu} from '../antd'
import moment, {Moment, unitOfTime} from 'moment'

const genRange = (unit: unitOfTime.DurationConstructor, previous?: boolean): [Moment, Moment] => [
	previous
		? moment()
			.subtract(1, unit)
			.startOf(unit)
		: moment()
			.subtract(1, unit)
			.startOf(unit),
	previous
		? moment()
			.subtract(1, unit)
			.endOf(unit)
		: moment()
			.subtract(1, unit)
			.endOf(unit),
]

const timeRanges: Array<[string, [Moment, Moment]]> = [
	['Yesterday', genRange('day', true)],
	['Today', genRange('day')],
	['Last Week', genRange('week', true)],
	['This Week', genRange('week')],
	['Last Month', genRange('month', true)],
	['This Month', genRange('month')],
	['Last Year', genRange('year', true)],
	['This Year', genRange('year')],
]

interface RelativeTimePickerProps {
	onChange?: Function
}

const TimeSelectionMenu = observer(({onClick}) => {
	const i18n = usei18n()
	return (
		<Menu onClick={onClick}>
			{timeRanges.map(([range, interval]) => (
				<Menu.Item key={range} interval={interval}>
					{i18n.get(range) || range}
				</Menu.Item>
			))}
		</Menu>
	)
})

@observer
export class RelativeTimePicker extends React.Component<RelativeTimePickerProps> {

	@observable selectedRange: string

	onClick = ({key}) => {
		this.selectedRange = key
		this.props.onChange && this.props.onChange(key, timeRanges[key])
	}

	render() {
		return (
			<Dropdown overlay={<TimeSelectionMenu onClick={this.onClick}/>}>
				<a>
					{this.selectedRange || 'Select a time range'}
					<Icon type='down' />
				</a>
			</Dropdown>
		)
	}

}
