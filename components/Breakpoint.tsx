import React from 'react'
import Responsive, {MediaQueryProps} from 'react-responsive'

const breakpoints = {
	xs: 480,
	sm: 768,
	md: 1024,
	lg: 1280,
	xl: 1440,
	xxl: 1920,
}
export function Breakpoint({xs, sm, md, lg, xl, xxl, only, ...props}): JSX.Element {
	const p: MediaQueryProps = {}
	if (xs) {
		p.maxWidth = breakpoints.xs
	} else if (sm) {
		p.minWidth = breakpoints.sm
		if (only) {
			p.maxWidth = breakpoints.md
		}
	} else if (md) {
		p.minWidth = breakpoints.md
		if (only) {
			p.maxWidth = breakpoints.lg
		}
	} else if (lg) {
		p.minWidth = breakpoints.lg
		if (only) {
			p.maxWidth = breakpoints.xl
		}
	} else if (xl) {
		p.minWidth = breakpoints.xl
		if (only) {
			p.maxWidth = breakpoints.xxl
		}
	} else if (xxl) {
		p.minWidth = breakpoints.xxl
	}
	return <Responsive {...p} {...props} />
}
