import React, {useState, useEffect} from 'react'
import {AutoComplete} from '../../../antd'
import {getTzData} from './api'

export function tz(props) {
	const [tzs, setTzs] = useState()

	useEffect(() => {
		if (!tzs) {
			getTzData().then(setTzs)
		}
	})

	return <AutoComplete dataSource={tzs} {...props} size='small' />
}
