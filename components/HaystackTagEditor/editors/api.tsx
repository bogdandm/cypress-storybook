// cache units to avoid subsequent frequent network requests
let cache = {}

export async function getUnits() {
	if (cache.units) {
		return cache.units
	}

	var {result} = await finstack.op('unitdb')

	cache.units = result.toObj()

	return cache.units
}

export async function getTzData() {
	if (cache.tz) {
		return cache.tz
	}

	var {result} = await finstack.op('tzdb')

	cache.tz = result.gridRows.map(r => r.tagsTable.fullName.value)

	return cache.tz
}
