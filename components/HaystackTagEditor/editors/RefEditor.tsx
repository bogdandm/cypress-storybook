import React, {useState} from 'react'
import {observer} from 'mobx-react'
import {usei18n} from '@j2inn/utils'
import {Icon, Button} from '../../../antd'
import {Container, Spacer} from '../../Container'
import {HaystackRecordSelector} from '../../HaystackRecordSelector'
import classnames from 'classnames'

import styles from './styles.styl'

interface RefEditorProps {
	value?: any;
	onChange?: Function;
	treeName?: string;
	className?: string
}

export const RefEditor = observer(({value, onChange, treeName, className, ...rest}: RefEditorProps) => {
	const [selection, setSelection] = useState()
	const i18n = usei18n()
	return (
		<Container vertical class={classnames(styles.RefEditor, className)} {...rest}>
			<HaystackRecordSelector
				value={value}
				onSelect={newSel => setSelection(newSel)}
				treeName={treeName}
				style={{width: '100%'}}
			/>
			<Container class={styles.controls} horizontal>
				<Button onClick={() => onChange(null)}>
					<Icon type='close-circle' />
					{i18n.get('Clear Ref')}
				</Button>
				<Spacer />
				<Button type='primary' onClick={() => onChange(selection)}>
					{i18n.get('Select')}
				</Button>
			</Container>
		</Container>
	)
})
