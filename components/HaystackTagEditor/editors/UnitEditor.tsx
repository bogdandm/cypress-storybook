import React, {useState, useEffect} from 'react'
import {usei18n} from '@j2inn/utils'
import {groupBy} from 'lodash'
import {Button, Popover} from '../../../antd'
import {CategoryPanel} from '../../CategoryPanel'

import {getUnits} from './api'

const columns = [
	{
		render(row) {
			return row.symbol
		},
	},
	{dataIndex: 'name'},
]

export function unit({value, onChange, buttonLabel}) {
	const [units, setUnits] = useState()
	const i18n = usei18n()

	if(!buttonLabel) {
		buttonLabel = i18n.get('Select unit')
	}

	useEffect(() => {
		if (!units) {
			getUnits().then(setUnits)
		}
	})

	const categories = units ? groupBy(units, 'quantity') : {}

	// map the unit symbol string to the actual object from units
	// so that CategoryPanel can find it and show that category as selected
	// by default
	if (value) {
		for (let category in categories) {
			const match = categories[category].find(v => v.symbol === value)
			if (match) {
				value = match
				break
			}
		}
	}

	return (
		<>
			{value ? (value.symbol ? value.symbol : value) : 'no unit'}
			<Popover
				trigger={['click']}
				content={
					<CategoryPanel
						value={value}
						categories={categories}
						columns={columns}
						onSelect={u => onChange(u.symbol)}
						footer={
							<Button size='small' onClick={() => onChange(null)}>
								{i18n.get('No Unit')}
							</Button>
						}
					/>
				}>
				<Button size='small' style={{marginLeft: '0.5em'}}>
					{buttonLabel}
				</Button>
			</Popover>
		</>
	)
}
