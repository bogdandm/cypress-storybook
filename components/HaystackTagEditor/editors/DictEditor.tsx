import React, {useState} from 'react'
import {Button, Select, Input, Table} from '../../../antd'
import {Container} from '../../Container'
import {Loadable} from '../../Loadable'

const HaystackTagEditor = Loadable(() => import('../index'))

import styles from './styles.styl'

const kinds = [
	'Ref',
	'Marker',
	'Number',
	'Str',
	'Bool',
	'Dict',
	'List',
	'Duration',
	'DateTime',
]

const columns = [
	{
		title: 'Name',
		render(tag) {
			return <Input value={tag.name} style={{flex: 1}} />
		},
	},
	{
		title: 'Kind',
		render(tag) {
			return (
				<Select value={tag.kind} style={{flex: 1, width: '100%'}}>
					{kinds.map(kind => (
						<Select.Option key={kind} value={kind}>
							{kind}
						</Select.Option>
					))}
				</Select>
			)
		},
	},
	{
		title: 'Value',
		render(tag) {
			return tag ? <HaystackTagEditor tag={tag} /> : '(empty)'
		},
	},
]

export function DictEditor({tag, style, tableStyle, ...props}) {
	return (
		<Container vertical class={styles.DictEditor} style={style}>
			<Table
				columns={columns}
				dataSource={tag.tags || []}
				size='small'
				style={tableStyle}
				{...props}
			/>
		</Container>
	)
}
