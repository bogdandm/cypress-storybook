import React from 'react'
import {usei18n} from '@j2inn/utils'
import {ColorPicker} from '../../ColorPicker'
import {Popover, Button} from '../../../antd'

import classes from './styles.styl'

export function ColorEditor({value, onChange: onChangeOriginal, ...props}) {
	const i18n = usei18n()
	if (onChangeOriginal) {
		var onChange = e => {
			onChangeOriginal(e.hex)
		}
	}
	function Pop({children}) {
		return (
			<Popover
				width={380}
				overlayClassName={classes.ColorEditor_popover}
				trigger={['click']}
				content={<ColorPicker {...props} onChange={onChange} />}>
				{children}
			</Popover>
		)
	}
	return (
		<>
			<Pop>
				<div
					class={classes.ColorEditor_color}
					style={{backgroundColor: value}}></div>
			</Pop>
			<span class={classes.ColorEditor_value}>{value}</span>
			<Pop>
				<Button size='small' style={{marginLeft: '0.5em'}}>
					{i18n.get('Select')}
				</Button>
			</Pop>
		</>
	)
}
