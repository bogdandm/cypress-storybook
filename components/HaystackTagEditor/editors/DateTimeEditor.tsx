import React from 'react'
import moment from 'moment'
import {DatePicker, TimePicker} from '../../../antd'

export function DateTimeEditor({value, ...props}) {
	return <DatePicker value={moment(value)} showTime {...props} size='small' />
}

export function TimeEditor({value, ...props}) {
	return (
		<TimePicker
			value={moment(value, 'hh:mm:ss')}
			showTime
			{...props}
			size='small'
		/>
	)
}
