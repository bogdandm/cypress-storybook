import React from 'react'
import {InputNumber} from '../../../antd'

export function CoordEditor({value, ...props}) {
	const [lat, lng] = value ? value.split(',') : [0, 0]
	function latChange(v) {
		props.onChange(`${v},${lng}`)
	}
	function lngChange(v) {
		props.onChange(`${lat},${v}`)
	}
	return (
		<>
			<InputNumber
				{...props}
				onChange={latChange}
				value={lat}
				size='small'
				precision={3}
				min={-90}
				max={90}
			/>

			<InputNumber
				{...props}
				onChange={lngChange}
				value={lng}
				size='small'
				precision={3}
				min={-90}
				max={90}
			/>
		</>
	)
}
