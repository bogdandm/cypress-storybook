import React, {useState} from 'react'
import {observer} from 'mobx-react'
import {usei18n} from '@j2inn/utils'
import {Modal, Button} from '../../../antd'
import {HaystackRecordEditorModal} from '../../HaystackRecordEditor'
import {HaystackGridTable} from '../../HaystackGridTable'
import {Container} from '../../Container'
import classes from './styles.styl'

const parse = actionString => {
	return fin.Haystack.DeserializeHaystack(actionString)
}

const serialize = grid => {
	return fin.Haystack.SerializeHaystack(grid)
}

export const actions = observer(({value, onChange, ...props}) => {
	const [grid, setGrid] = useState(parse(value))
	const i18n = usei18n()
	const cols = [
		{
			render(record) {
				return (
					<HaystackRecordEditorModal
						record={record}
						trigger={<Button size='small'>{i18n.get('Edit') || 'Edit'}</Button>}
						onSave={rec => {
							const rows = grid.gridRows
							const index = rows.indexOf(rec)
							setGrid(
								fin.Haystack.HaystackGrid.BuildFromRows([
									...rows.slice(0, index),
									record,
									...rows.slice(index + 1),
								])
							)
							console.log(rec)
						}}
					/>
				)
			},
		},
	]
	return (
		<Container left horizontal nowrap class={classes.ActionsEditor}>
			<span>{value}</span>
			<Modal
				title={i18n.get('Edit actions')}
				onOk={() => onChange(serialize(grid))}
				onCancel={() => setGrid(serialize(grid))}
				class={classes.ActionsEditorModal}
				rowKey='displayName'
				trigger={
					<Button size='small' style={{marginLeft: '0.5em'}}>
						{i18n.get('Edit') || 'Edit'}
					</Button>
				}>
				<HaystackGridTable grid={grid} size='small' columns={cols} />
			</Modal>
		</Container>
	)
})
