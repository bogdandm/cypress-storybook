import React, {useState} from 'react'
import {observer} from 'mobx-react'
import {usei18n} from '@j2inn/utils'
import {Modal, Button, Input, InputNumber, Icon, Tooltip} from '../../../antd'
import {Container} from '../../Container'
import classes from './styles.styl'

const byIndex = (a, b) => a.index - b.index
const idGen = () => ((Math.random() * 0xffffff) << 0).toString(16)

const parseEnums = enumString => {
	return enumString
		.split(',')
		.map((value, index) => ({id: idGen(), index, value}))
}

const serializeEnums = enums => {
	enums.sort(byIndex)
	const highest = enums[enums.length - 1].index
	return [...new Array(highest + 1)]
		.map((_, i) => {
			const e = enums.find(en => en.index === i)
			return e ? e.value : ''
		})
		.join(',')
}

export const __enum = observer(({value, onChange}) => {
	const [enums, setEnums] = useState(parseEnums(value))
	const i18n = usei18n()
	return (
		<Container left horizontal nowrap class={classes.EnumEditor}>
			<span>{value === '' ? <em>empty</em> : value}</span>
			<Modal
				title={i18n.get('Edit enum')}
				class={classes.EnumEditorModal}
				trigger={<Button size='small'>{i18n.get('Edit')}</Button>}
				onCancel={() => setEnums(parseEnums(value))}
				onOk={() => onChange && onChange(serializeEnums(enums))}>
				<table>
					<thead>
						<th>Index</th>
						<th>Value</th>
						<th></th>
					</thead>
					<tbody>
						{enums.map(enu => {
							const {value: val, index, id} = enu
							return (
								<tr key={id}>
									<td>
										<InputNumber
											onChange={v => {
												enu.index = v
												setEnums([...enums])
											}}
											value={index}
											min={0}
										/>
									</td>
									<td
										style={{
											width: '100%',
										}}>
										<Input
											onChange={e => {
												enu.value = e.target.value
												setEnums([...enums])
											}}
											value={val}
										/>
									</td>
									<td>
										<Tooltip title={`${i18n.get('Remove')} v${index}`}>
											<Icon
												type='close'
												style={{cursor: 'pointer'}}
												onClick={() => {
													setEnums([
														...enums.slice(
															0,
															index
														),
														...enums.slice(
															index + 1
														),
													])
												}}
											/>
										</Tooltip>
									</td>
								</tr>
							)
						})}
					</tbody>
				</table>
				<Button
					size='small'
					icon='plus'
					type={'dashed'}
					block
					onClick={() => {
						const n = [...enums].sort(byIndex)
						n.push({
							id: idGen(),
							index: n[n.length - 1].index + 1,
							value: '',
						})
						setEnums(n)
					}}
					class={classes.EnumAddButton}>
					{i18n.get('Add value')}
				</Button>
			</Modal>
		</Container>
	)
})
