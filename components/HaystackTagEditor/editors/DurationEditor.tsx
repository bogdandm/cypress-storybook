import React, {useState} from 'react'
import {observer} from 'mobx-react'
import {usei18n} from '@j2inn/utils'
import {Select, Input} from '../../../antd'

const {Option} = Select

const timeUnits = ['ms', 'sec', 'min', 'hr', 'day', 'wk', 'mo', 'yr']

const preventNonDigits = e => {
	if (!/[0-9]/.test(e.key)) {
		e.preventDefault()
		return false
	}
}

export const DurationEditor = observer(({tag, value, ...props}) => {
	const [currentUnit, setCurrentUnit] = useState(tag.unit || 'hr')
	const i18n = usei18n()
	const timeUnitSelect = (
		<Select
			value={currentUnit}
			onChange={v => setCurrentUnit(v)}
			style={{width: 60}}
			size='small'>
			{timeUnits.map(u => (
				<Option key={u}>{i18n.get(u) || u}</Option>
			))}
		</Select>
	)
	return (
		<Input
			{...props}
			value={value}
			size='small'
			addonAfter={timeUnitSelect}
			onKeyDown={preventNonDigits}
		/>
	)
})
