import React, {useState} from 'react'
import {unit as UnitEditor} from './UnitEditor'
import {Popover, Button, Icon, InputNumber} from '../../../antd'

export function NumberEditor({tag, ...props}) {
	const [currentUnit, setCurrentUnit] = useState(tag.unit)
	return (
		<>
			<InputNumber
				{...props}
				size='small'
				style={{marginRight: '0.5em'}}
			/>
			<UnitEditor
				value={currentUnit}
				onChange={unit => {
					tag.unit = unit
					setCurrentUnit(unit)
				}}
			/>
		</>
	)
}
