import React, {useState} from 'react'
import {Input, Switch} from '../../antd'
import {Container} from '../Container'

import * as editors from './editors'

const {
	DateTimeEditor,
	TimeEditor,
	NumberEditor,
	DurationEditor,
	ColorEditor,
	CoordEditor,
	DictEditor,
	RefEditor,
} = editors

function noop() {}

const colorRegex = /(?:#[a-f\d]{6,8}|rgba*\(.+?\)|hsla*\(.+?\))/i

export default function HaystackTagEditor({tag, value, onChange, ...props}) {
	const [currentValue, setCurrentValue] = useState(tag.value)

	const onChangeShim = onChange
		? e => {
			let v = e
			if (v) {
					// if an event is passed, extract the value from target
				if (v.target) {
					v = v.target.value
				}

					// if a moment object is passed, convert to js date
					// and don't pass the moment() object
				if (v && v._isAMomentObject) {
					e = v = v.toDate()
				}
			}

				// Now pass the normalized value to the onChange prop
			onChange && onChange(v, e)

				// Finally keep our internal state in sync
			setCurrentValue(v)
		  }
		: noop

	const common = {
		value: currentValue,
		onChange: onChangeShim,
		tag,
		...props,
	}

	// Display the name-specific editor if there is one
	// Some tag names are reserved keywords in js, so they get
	// prefixed with __ e.g. `__enum` from EnumEditor.js
	if (editors[tag.name] || editors['__' + tag.name]) {
		const Editor = editors[tag.name] || editors['__' + tag.name]
		return <Editor {...common} />
	}

	const isColor = colorRegex.test(tag.value)

	if (isColor) {
		return <ColorEditor {...common} />
	}

	switch (tag.kind) {
	case 'Ref':
		return <RefEditor {...common} />
	case 'Number':
		return <NumberEditor {...common} />
	case 'Str':
		return <Input.TextArea {...common} autosize size='small' />
	case 'Bool':
		return (
			<Switch
				checkedChildren='True'
				unCheckedChildren='False'
				style={{width: 65}}
			/>
		)
	case 'Dict':
		return <DictEditor {...common} />
	case 'List':
		return (
			<Container vertical>
				{tag.value && tag.value.map
					? tag.value.map((t, i) => (
						<HaystackTagEditor key={i} tag={t} />
						  ))
					: null}
			</Container>
		)
	case 'Duration':
		return <DurationEditor {...common} />
	case 'DateTime':
		return <DateTimeEditor {...common} />
	case 'Time':
		return <TimeEditor {...common} />
	case 'Coord':
		return <CoordEditor {...common} />
	}

	return null
}
