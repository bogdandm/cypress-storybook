import React from 'react'
import {storiesOf} from '@storybook/react'
import {withKnobs, select} from '@storybook/addon-knobs'
import {action} from '@storybook/addon-actions'

import HaystackTagEditor from './index'

const props = {
	onChange: action('onChange'),
}

const s = storiesOf('Haystack Tag Editor', module).addDecorator(withKnobs)

Object.values(fin.Haystack.TagKind).forEach(kind =>
	s.add(kind, () => {
		const tag = fin.Haystack.GenerateTag('test', kind)
		return <HaystackTagEditor tag={tag} {...props} />
	})
)
