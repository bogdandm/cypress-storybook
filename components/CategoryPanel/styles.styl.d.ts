// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'categories': string;
  'category': string;
  'controls': string;
  'selected': string;
  'tableContainer': string;
  'values': string;
  'wrapper': string;
}
export const cssExports: CssExports;
export default cssExports;
