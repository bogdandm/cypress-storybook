import React, {useState, useEffect, useRef} from 'react'
import {observer} from 'mobx-react'
import {usei18n} from '@j2inn/utils'
import {Button, Input, Icon} from '../../antd'
import {Container, Spacer} from '../Container'
import {pickBy, startsWith} from 'lodash'
import Highlighter from 'react-highlight-words'
import classnames from 'classnames'

import styles from './styles.styl'

export const CategoryPanel = observer(({
	searchString,
	value,
	categories,
	columns,
	onSelect,
	footer,
	omitHighlight,
}) => {
	const [search, setSearch] = useState(searchString)
	const [filteredCategories, setFilteredCategories] = useState()
	const [filteredValues, setFilteredValues] = useState()
	const [selectedCategory, setSelectedCategory] = useState()
	const [selectedValue, setSelectedValue] = useState(value)

	const categoriesEl = useRef(null)
	const valuesEl = useRef(null)
	const i18n = usei18n()

	// Filter out left side categories
	useEffect(() => {
		const newSet = pickBy(categories, (items, category) => {
			if (search) {
				return (
					startsWith(category.toLowerCase(), search.toLowerCase()) ||
					items.some(obj =>
						Object.keys(obj).some(key =>
							obj[key]
								? startsWith(
									obj[key].toLowerCase(),
									search.toLowerCase()
								  )
								: false
						)
					)
				)
			}
			return true
		})
		if (!newSet[selectedCategory] || !selectedCategory) {
			setSelectedCategory(Object.keys(newSet)[0])
		}
		setFilteredCategories(newSet)
	}, [search, categories])

	// Filter out right side values
	useEffect(() => {
		if (!selectedCategory || !filteredCategories) {
			return
		}
		const filtered = filteredCategories[selectedCategory].filter(items => {
			if (search) {
				return Object.keys(items).some(key =>
					items[key]
						? startsWith(
							items[key].toLowerCase(),
							search.toLowerCase()
						  )
						: false
				)
			}
			return true
		})
		setFilteredValues(filtered)
	}, [search, selectedCategory, filteredCategories])

	// set the default category
	useEffect(() => {
		if (value && categories && !selectedCategory) {
			for (let category in categories) {
				const match = categories[category].find(v => v === value)
				if (match) {
					setSelectedCategory(category)
					setSelectedValue(match)
					const el = categoriesEl.current.querySelector(
						`[data-category="${category}"]`
					)
					if (el) {
						el.scrollIntoView()
					}
					//TODO auto scroll to selected value?
					//valuesEl
					break
				}
			}
		}
	}, [value, categories])

	return (
		<Container vertical class={styles.wrapper}>
			<Input.Search
				placeholder={i18n.get('Search') + '...'}
				value={search}
				autoFocus={true}
				onChange={e => setSearch(e.target.value)}
			/>
			<Container top horizontal nowrap class={styles.tableContainer}>
				<div class={styles.categories} ref={categoriesEl}>
					{Object.keys(filteredCategories || categories || []).map(
						category => (
							<div
								data-category={category}
								key={category}
								onClick={() => setSelectedCategory(category)}
								class={classnames(styles.category, {
									[styles.selected]:
										selectedCategory === category,
								})}>
								<Icon
									type={
										selectedCategory === category
											? 'folder-open'
											: 'folder'
									}
								/>
								{!omitHighlight && (
									<Highlighter
										highlightClassName={styles.highlight}
										searchWords={[search]}
										autoEscape={true}
										textToHighlight={category}
									/>
								)}
								{omitHighlight && <span>{category}</span>}
								{selectedCategory === category && (
									<Icon type='right' />
								)}
							</div>
						)
					)}
				</div>
				<div class={styles.values} ref={valuesEl}>
					<table>
						<tbody>
							{filteredValues &&
								filteredValues.map((items, r) => (
									<tr
										key={r}
										class={classnames({
											[styles.selected]:
												selectedValue === items,
										})}
										onClick={() => setSelectedValue(items)}>
										{columns.map((column, c) => (
											<td key={c}>
												{!omitHighlight && (
													<Highlighter
														highlightClassName={
															styles.highlight
														}
														searchWords={[search]}
														autoEscape={true}
														textToHighlight={
															column.render
																? column.render(
																	items
																  )
																: items[
																	column
																		.dataIndex
																  ]
														}
													/>
												)}
												{omitHighlight &&
												column.render ? (
														column.render(items)
													) : (
														<span>
															{
																items[
																	column.dataIndex
																]
															}
														</span>
													)}
											</td>
										))}
									</tr>
								))}
						</tbody>
					</table>
				</div>
			</Container>
			<Container horizontal class={styles.controls}>
				{footer}
				<Spacer />
				<Button
					type='primary'
					disabled={!selectedValue}
					onClick={() => onSelect(selectedValue)}>
					{i18n.get('Select')}
				</Button>
			</Container>
		</Container>
	)
})
