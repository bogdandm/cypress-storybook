import React from 'react'
import {Tag} from '../../antd/Tag'
import {TagValue} from '../HaystackRecordEditor/TagValue'

export function RecordInfo({record}) {
	return (
		<table>
			<tbody>
				<tr key='markers'>
					<td>Markers</td>
					<td>
						{record.tags
							.filter(tag => tag.kind === 'Marker')
							.map(tag => (
								<Tag
									color='blue'
									key={tag.name}
									style={{marginTop: 5, marginBottom: 5}}>
									{tag.name}
								</Tag>
							))}
					</td>
				</tr>
				{record.tags
					.filter(tag => tag.kind !== 'Marker')
					.map(tag => (
						<tr key={tag.name}>
							<td>{tag.name}</td>
							<td>
								<TagValue tag={tag} />
							</td>
						</tr>
					))}
			</tbody>
		</table>
	)
}
