import React, {useState, useEffect} from 'react'
import {storiesOf} from '@storybook/react'
import {withKnobs, text} from '@storybook/addon-knobs'
import {action} from '@storybook/addon-actions'

import {HaystackGridTable} from './index'

const query = async (q, cb) => {
	const {result} = await finstack.eval(q)
	window.result = result
	cb(result)
}

function DataProvider({children}) {
	const [grid, setGrid] = useState()
	//const [axonquery, setAxonQuery] = useState('readAll(site)')

	const axon = text('Axon query', 'readAll(point)')

	useEffect(() => {
		query(axon, setGrid)
	}, [axon])

	return (
		<div style={{padding: '1em'}}>
			{React.cloneElement(children, {
				grid: grid ? grid : null,
				onChange: action('onChange'),
			})}
		</div>
	)
}

storiesOf('Haystack Grid Table', module)
	.addDecorator(withKnobs)
	.add('basic', () => {
		return (
			<DataProvider>
				<HaystackGridTable />
			</DataProvider>
		)
	})
