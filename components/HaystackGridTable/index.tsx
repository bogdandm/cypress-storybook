import React from 'react'
import {observer, Observer} from 'mobx-react'
import {observable, computed, reaction} from 'mobx'
import Highlighter from 'react-highlight-words'
import {isNil, isString} from 'lodash'
import {
	Table,
	Input,
	Icon,
	Button,
	Pagination,
	Popover,
	Tooltip,
	Tag,
} from '../../index'
import {RecordInfo} from './RecordInfo'

import styles from './style.styl'

interface HaystackGridTableProps {
	grid: unknown, // use HaystackGrid Model
	hideColumns?: string[],
	showColumns?: string[],
	showHeader?: boolean,
	size?: string,
	onRow?: (unknown) => unknown, // TODO better typing
	selectedItems?: string[],
	showOnlySelectedItems?: boolean,
	onRefClick?: Function,
	onDataChange?: Function
	hidePagination?: boolean
	// TODO missing prop typings, complete other prop typings
}

@observer
export class HaystackGridTable extends React.Component<HaystackGridTableProps, any> {

	@observable filters = {}
	@observable sort = {
		column: null,
		order: 'descend',
	}

	@observable searchText
	@observable page = 1
	@observable pageSize = 20

	@observable selectedMarkers = []

	markerColorCache = {}
	lastUsedMarkerColor = 0

	searchInput = React.createRef()

	getColumnSearchProps = field => ({
		filterDropdown: ({
			setSelectedKeys,
			selectedKeys,
			confirm,
			clearFilters,
		}) => (
			<div style={{padding: 8}}>
				<Input
					ref={this.searchInput}
					placeholder={`Search ${field}`}
					value={selectedKeys[0]}
					onChange={e =>
						setSelectedKeys(e.target.value ? [e.target.value] : [])
					}
					onPressEnter={() =>
						this.handleSearch(selectedKeys, confirm)
					}
					style={{width: 188, marginBottom: 8, display: 'block'}}
				/>
				<Button
					type='primary'
					onClick={() => this.handleSearch(selectedKeys, confirm)}
					icon='search'
					size='small'
					style={{width: 90, marginRight: 8}}>
					Search
				</Button>
				<Button
					onClick={() => this.handleReset(clearFilters)}
					size='small'
					style={{width: 90}}>
					Reset
				</Button>
			</div>
		),
		filterIcon: filtered => (
			<Icon
				type='search'
				style={{color: filtered ? '#1890ff' : undefined}}
			/>
		),
		onFilter: (value, record) => {
			if (!record || !record.tagsTable[field]) {
				return
			}
			return record.tagsTable[field]
				.toString()
				.toLowerCase()
				.includes(value.toLowerCase())
		},
		onFilterDropdownVisibleChange: visible => {
			if (visible) {
				//eslint-disable-next-line react/no-find-dom-node
				//setTimeout(() => findDOMNode(this.searchInput.current).focus())
			}
		},
		render: record => {
			if (!record || !record.tagsTable || !record.tagsTable[field]) {
				return
			}
			const text = record.tagsTable[field].toString()
			return (
				<Highlighter
					highlightStyle={{backgroundColor: '#ffc069', padding: 0}}
					searchWords={[this.searchText]}
					autoEscape
					textToHighlight={text}
				/>
			)
		},
	})

	handleSearch = (selectedKeys, confirm) => {
		confirm()
		this.searchText = selectedKeys[0]
	}

	handleReset = clearFilters => {
		clearFilters()
		this.searchText = ''
	}

	clearSelectedMarkers = () => {
		this.selectedMarkers = []
	}

	getRecordDisplayName(r) {
		if (r.displayName && r.displayName.length > 0) {
			return r.displayName
		} else if (r.dis && r.dis.length > 0) {
			return r.dis
		} else if (r.getTag('name') && r.getTag('name').value.length > 0) {
			return r.getTag('name').value
		} else if (
			r.getTag('navName') &&
			r.getTag('navName').value.length > 0
		) {
			return r.getTag('navName').value
		} else if (r.getTag('id') && r.getTag('id').value.length > 0) {
			return r.getTag('id').value
		} else {
			return ''
		}
	}

	onRefClick(ref) {
		if (this.props.onRefClick) {
			this.props.onRefClick(ref)
		}
	}

	@computed get grid() {
		return this.props.grid
	}

	@computed get rows() {
		if (!this.grid || !this.grid.gridRows) {
			return []
		}
		//	allows for only the selected items to be shown
		if (this.props.rowSelection && this.props.showOnlySelectedItems) {
			return this.props.selectedItems
		}
		return this.grid.gridRows.filter(row => {
			if (!this.selectedMarkers.every(marker => row.tagsTable[marker])) {
				return false
			}
			return true
		})
	}

	@computed get columns() {
		if (!this.grid || !this.grid.columns) {
			return []
		}
		let columns = this.grid.columns
		let markerColumns = columns.filter(c => c.valueType === 'Marker')

		return [
			{
				key: 'dis',
				title: 'dis',
				width: 200,
				render: record => {
					return (
						<>
							<Popover
								content={<RecordInfo record={record} />}
								placement='right'
								trigger='click'
								overlayClassName={styles.RecordInfo}>
								<Icon
									type='info-circle'
									style={{
										marginRight: '0.5rem',
										pointer: 'cursor',
									}}
								/>
							</Popover>
							{this.getRecordDisplayName(record)}
						</>
					)
				},
			},
			{
				key: `markers_${Math.random()
					.toString(36)
					.substr(2, 9)}`,
				title: 'Markers'/*(
					<>
						{this.selectedMarkers.length > 1 && (
							<Tooltip title='Clear marker filter'>
								<Icon
									type='stop'
									onClick={this.clearSelectedMarkers}
									style={{float: 'right', opacity: 0.5}}
								/>
							</Tooltip>
						)}
					</>
				)*/,
				filters: markerColumns.map(({name}) => ({
					text: name,
					value: name,
				})),
				onFilter: (val, record) => record.tagsTable[val],
				render: record => {
					let letters = 0,
						cutoffIndex = 0
					// only show markers that are on this record
					const markers = markerColumns
						.filter(col => record.tagsTable[col.name])
						.sort((a, b) => a.name.localeCompare(b.name))
					return (
						<Observer>
							{() => (
								<>
									{markers
										.filter((col, index) => {
											const name =
												record.tagsTable[col.name].name
											if (letters > 15) {
												return
											}
											if (letters + name.length > 15) {
												cutoffIndex = index
											}
											letters += name.length
											return true
										})
										.map(marker => (
											<Tag
												key={marker.name}
												color='blue'
												class={styles.marker}
												style={
													this.selectedMarkers
														.length > 0 &&
													!this.selectedMarkers.includes(
														marker.name
													)
														? {opacity: 0.2}
														: null
												}
												onClick={() => {
													const index = this.selectedMarkers.indexOf(
														marker.name
													)
													if (index > -1) {
														this.selectedMarkers.splice(
															index,
															1
														)
													} else {
														this.selectedMarkers.push(
															marker.name
														)
													}
												}}>
												{marker.name}
											</Tag>
										))}
									<Tooltip
										overlayClassName={styles.markerPopup}
										title={markers
											.slice(cutoffIndex + 1)
											.map(marker => (
												<Tag
													key={marker.name}
													color='blue'>
													{marker.name}
												</Tag>
											))}>
										{markers.length - cutoffIndex - 1 >
											0 && (
											<Tag color='blue'>
												+{' '}
												{markers.length -
													cutoffIndex -
													1}
											</Tag>
										)}
									</Tooltip>
								</>
							)}
						</Observer>
					)
				},
			},
		]
			.concat(
				columns
					.filter(
						c =>
							c.valueType !== 'Marker' &&
							c.name !== 'id' &&
							c.name !== 'actions' &&
							c.name !== 'dis' //	because the dis column is already defined and this leads to duplicate column and React throwing a slew of warnings
					)
					.map((col, i, cols) => {
						//	thanks MDN :)
						const circularReplacer = () => {
							const seen = new WeakSet()
							return (key, value) => {
								if (typeof value === 'object' && value !== null) {
									if (seen.has(value)) {
										return
									}
									seen.add(value)
								}
								return value
							}
						}
						const stringify = obj => {
							if (typeof obj !== 'object' || Array.isArray(obj)) {
								return JSON.stringify(obj, circularReplacer())
							}

							let props = Object.keys(obj)
								.map(key => `${key}: ${stringify(obj[key])}`)
								.join(', ')
							return `{${props}}`
						}

						const ret = {
							key: col.name,
							title: col.name,
							...this.getColumnSearchProps(col.name),
							width: i === cols.length - 1 ? undefined : 450,
							sorter: (a, b) => {
								a = a.tagsTable[col.name]
								b = b.tagsTable[col.name]

								a = a && a.value ? a.value : a
								b = b && b.value ? b.value : b

								if (a === b) {
									return 0
								} else if (isNil(a)) {
									return 1
								} else if (isNil(b)) {
									return -1
								}
								if (isString(a) && isString(b)) {
									return a.localeCompare(b)
								}
								return a - b
							},
							defaultSortOrder: 'descend',
						}

						if (col.valueType === 'Ref') {
							ret.render = record => {
								const ref = record.tagsTable[col.name]
								if (ref && ref.value) {
									return (
										<div style={{display: 'flex', alignItems: 'center'}}>
											<span style={{flex: '1 1 auto'}}>{ref.displayName}</span>
											<Button
												shape='circle'
												icon='arrow-right'
												size='small'
												style={{marginLeft: 5}}
												onClick={() => this.onRefClick(ref)}/>
										</div>
									)
								}
							}
						} else if (col.valueType === 'Duration') {
							ret.render = record => {
								const val = record.tagsTable[col.name]
								return val && val.original ? val.original : ''
							}
						} else if (
							col.valueType === 'Dict' ||
							col.valueType === 'List'
						) {
							ret.render = record => {
								const val = record.tagsTable[col.name]
								return val && 'toObj' in val
									? stringify(val.toObj())
									: '[Object]'
							}
						}

						return ret
					})
			)
			.filter(column => {
				if (this.props.showColumns) {
					return this.props.showColumns.includes(column.title)
				}
				if (this.props.hideColumns) {
					return !this.props.hideColumns.includes(column.title)
				}
				return true
			})
			.concat(this.props.columns || [])
	}

	componentDidMount() {
		if (DEBUG) {
			window.HaystackGrid = this
		}
		//	adds a callback hook for times when the
		//	rows are filtered via the clicking of markers
		//	or when only selection is shown
		reaction(
			() => this.rows && this.rows.length,
			() => {
				if (this.props.onDataChange) {
					this.props.onDataChange(this)
				}
			}
		)
	}

	onTableChange = (pagination, filters, sorter, extra) => {
		this.sort.column = sorter.columnKey
		this.sort.order = sorter.ascend
		this.filters = filters

		//	adds callback hook when the change is to be observed
		if (this.props.onDataChange) {
			this.props.onDataChange(this, pagination, filters, sorter, extra)
		}
	}

	onPaginationChange = page => (this.page = page)

	render() {
		const {
			columns = [],
			rows,
			props,
			onTableChange,
			onPaginationChange,
		} = this
		return (
			<>
				<div class={styles.wrapper}>
					<Table
						bordered
						size='small'
						dataSource={rows}
						className={styles.table}
						rowKey='id'
						pagination={{
							current: this.page,
							pageSize: 25,
							style: {
								display: 'none',
							},
						}}
						onChange={onTableChange}
						{...props}
						columns={columns}
					/>
				</div>
				{rows &&
					rows.length > 0 &&
					rows.length > this.pageSize &&
					!props.hidePagination && (
					<Pagination
						size='small'
						pageSize={this.pageSize}
						total={rows.length}
						onChange={onPaginationChange}
						style={{marginTop: '1em'}}
					/>
				)}
			</>
		)
	}

}
