import React from 'react'
import {storiesOf} from '@storybook/react'
import {withKnobs, select, text} from '@storybook/addon-knobs'
import {action} from '@storybook/addon-actions'

import {NavTree} from './index'

const trees = [
	'treeName:equip',
	'connExts()',
	'dHJ5IGZpbkZpbHRlcmVkRnVuY0V2YWwocmVsYXRpb25zaGlwRnVuYywgY2hpbGRyZW5GaWx0ZXIgYW5kIHJlZkZpbHRlcikuYWRkUm93cyhyZWFkQWxsKHJlbGF0aW9uc2hpcCBvciByZWxhdGlvbnNoaXBGb2xkZXIpKS5maW5kQWxsKHYgPT4gdi5oYXMoInJlbGF0aW9uc2hpcEZvbGRlciIpIG9yIHYtPnJlZkZpbHRlci5wYXJzZUZpbHRlci5yZWFkQ291bnQgPiAwKSBjYXRjaCBbXQ==',
	'custom',
]

storiesOf('Haystack Nav Tree', module)
	.addDecorator(withKnobs)
	.add('basic', () => {
		const props = {
			treeName: select('Trees', trees, trees[0]),
		}
		if (props.treeName == 'custom') {
			props.treeName = text('Custom tree', 'treeName:equip')
		}
		return <NavTree {...props} onChange={action('onChange')} />
	})
