import React, {useState, useEffect} from 'react'
import {Tree} from '../../antd'
import {isArray} from 'lodash'
import {query} from '@j2inn/utils'

//import styles from './styles.styl'

const {TreeNode} = Tree

const getTreeData = async ({treeName = 'treeName:equip', node}) => {
	let {result} = await query(
		`finNavListChildren("${treeName}"${node ? `, "${node.treePath}"` : ''})`
	)
	result = result.toObj()
	return result
}

function titleFor(item) {
	return item.dis || item.navName || item.name || item.id || '---'
}

function renderTreeNodes(data) {
	return data.map(item => {
		if (
			item.children &&
			isArray(item.children) &&
			item.children.length > 0
		) {
			return (
				<TreeNode title={titleFor(item)} key={item.id} dataRef={item}>
					{renderTreeNodes(item.children)}
				</TreeNode>
			)
		}
		return (
			<TreeNode
				title={titleFor(item)}
				key={item.id}
				dataRef={item}
				isLeaf={!item.isBranch}
			/>
		)
	})
}

export function NavTree({treeName, value, onSelect, ...props}) {
	const [treeData, setTreeData] = useState()

	useEffect(() => {
		getTreeData({treeName}).then(setTreeData)
	}, [value, treeName])

	async function onLoadData(node) {
		const children = await getTreeData({
			treeName,
			node: node.props.dataRef,
		})
		node.props.dataRef.children = children
		setTreeData(treeData.slice())
	}

	// TODO Create a multiple mode that allows selection of multiple records from any level
	// If multiple return a <TreeSelect> and return the entire records
	// instead of the ids
	// * look at using treePath as key instead of id

	return (
		<Tree
			loadData={onLoadData}
			onSelect={keys => {
				onSelect && onSelect(keys.filter(i => i))
			}}
			defaultSelectedKeys={[value]}
			selectedKeys={[value]}
			{...props}>
			{renderTreeNodes(treeData || [])}
		</Tree>
	)
}
