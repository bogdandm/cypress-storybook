import React, {useState, useEffect} from 'react'

import {storiesOf} from '@storybook/react'
import {action} from '@storybook/addon-actions'
import {withKnobs, text} from '@storybook/addon-knobs'

import {
	HaystackRecordEditor,
	HaystackRecordEditorModal,
	HaystackRecordEditorDrawer,
} from './index'

const query = async (q, cb) => {
	const {result} = await finstack.eval(q)
	window.result = result
	cb(result)
}

function DataProvider({children}) {
	const [grid, setGrid] = useState()
	//const [axonquery, setAxonQuery] = useState('readAll(site)')

	//const axon = text('Axon query', 'readAll(point)[1]')
	const axon = text('Axon query', 'read(site)')

	useEffect(() => {
		query(axon, setGrid)
	}, [axon])

	return (
		<div style={{padding: '1em'}}>
			{React.cloneElement(children, {
				record: grid ? grid.gridRows[0] : null,
				onChange: action('onChange'),
			})}
		</div>
	)
}

storiesOf('Haystack Record Editor', module)
	.addDecorator(withKnobs)
	.add('standalone', () => (
		<DataProvider>
			<HaystackRecordEditor />
		</DataProvider>
	))
	.add('as modal', () => (
		<DataProvider>
			<HaystackRecordEditorModal />
		</DataProvider>
	))
	.add('as drawer', () => (
		<DataProvider>
			<HaystackRecordEditorDrawer />
		</DataProvider>
	))
