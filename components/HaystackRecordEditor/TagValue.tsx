import React from 'react'
import {Badge, Popover, Typography, Icon} from '../../antd'
import classnames from 'classnames'
import classes from './style.styl'

const {Text} = Typography

function TagIDInfo({id}) {
	return (
		<Popover mouseLeaveDelay={1} content={<Text copyable>{id}</Text>}>
			<Icon type='info-circle' style={{marginRight: '0.5em'}} />
		</Popover>
	)
}

export function TagValue({tag}) {
	const value =
		tag.kind === 'Marker' ? (
			<Badge title='Marker' color='cyan' />
		) : (
			fin.Haystack.SerializeHaystack(tag)
		)
	return (
		<span class={classnames(classes.tagValue, tag.kind)}>
			{tag.kind === 'Ref' && tag.name !== 'id' && (
				<TagIDInfo id={value} />
			)}
			{tag.name === 'id' ? (
				<Text copyable>{value}</Text>
			) : tag.kind === 'Dict' ? (
				value
			) : (
				tag.displayName || value || '---'
			)}
		</span>
	)
}
