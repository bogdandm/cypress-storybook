import React, {useState, useEffect} from 'react'
import {usei18n} from '@j2inn/utils'
import {observer} from 'mobx-react'
import {without} from 'lodash'
import {Icon, Input, Tag} from '../../antd'
import {isMarkerTag} from './utils'
import classes from './style.styl'

function MarkerTag({tag, children, ...props}) {
	return (
		<Tag visible title={tag.name} {...props}>
			<span>{children}</span>
		</Tag>
	)
}

export const Markers = observer(({markers, record, ...props}) => {
	const [inputVisible, setInputVisible] = useState(false)
	const [inputValue, setInputValue] = useState('')
	const [deletedMarkers, setDeletedMarkers] = useState([])
	const [originalMarkers, setOriginalMarkers] = useState()
	const i18n = usei18n()

	if (!originalMarkers) {
		setOriginalMarkers(markers)
	}

	const markTagForDeletion = tag => {
		setDeletedMarkers([...new Set([...deletedMarkers, tag.name])])
	}

	const undeleteTag = tag => {
		setDeletedMarkers(without(deletedMarkers, tag.name))
	}

	const addNewMarkerTag = () => {
		if (!inputValue) {
			return
		}
		props.onAddNewMarkerTag(inputValue)
		setInputValue('')
	}

	useEffect(() => {
		// remove all marker tags
		record.tags.filter(isMarkerTag).forEach(t => record.removeTag(t.name))
		// add back what should be there
		markers
			.filter(m => !deletedMarkers.includes(m.name))
			.forEach(m =>
				record.addTag(
					[...originalMarkers, ...markers].find(
						om => om.name === m.name
					)
				)
			)
	}, [deletedMarkers])

	return (
		<div class={classes.markers}>
			{markers.map((tag, i) =>
				deletedMarkers.includes(tag.name) ? (
					<MarkerTag
						key={`${i}-${tag.name}`}
						tag={tag}
						color='red'
						onClick={() => undeleteTag(tag)}
						style={{
							textDecoration: 'line-through',
							opacity: 0.3,
						}}>
						{tag.name}
					</MarkerTag>
				) : (
					<MarkerTag
						key={`${i}-${tag.name}`}
						tag={tag}
						color='blue'
						closable
						onClose={() => markTagForDeletion(tag)}>
						{tag.name}
					</MarkerTag>
				)
			)}
			{inputVisible && (
				<Input
					type='text'
					size='small'
					style={{width: 78}}
					value={inputValue}
					onChange={e => setInputValue(e.target.value)}
					onBlur={() => setInputVisible(false)}
					onPressEnter={addNewMarkerTag}
					autoFocus
				/>
			)}
			{!inputVisible && (
				<Tag
					onClick={() => setInputVisible(true)}
					style={{
						background: '#fff',
						borderStyle: 'dashed',
						cursor: 'pointer',
					}}>
					<Icon type='plus' /> {i18n.get('Marker Tag')}
				</Tag>
			)}
		</div>
	)
})
