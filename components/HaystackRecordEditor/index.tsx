import React, {useState, useEffect, useCallback, useMemo} from 'react'
import {observer} from 'mobx-react'
import {Modal, Drawer, Button, Divider, Dropdown, Menu, Icon} from '../../antd'
import {Container, Spacer} from '../Container'
import {AddTag} from './AddTag'
import {Markers} from './Markers'
import {TagTable} from './TagTable'
import {usei18n, useFinCommit} from '@j2inn/utils'
import {
	isMarkerTag,
	isNotMarkerTag,
	tagsOf,
	useTiles,
} from './utils'

type HaystackRecordEditorProps = {
	record: any, //TODO change to HaystackGridGrow
	additionalTiles?: Function[],
}

export const HaystackRecordEditor = observer(props => {
	if(!props.record) {
		console.warn('No record passed to HaystackRecordEditor')
		return null
	}
	return <HaystackRecordEditorInner {...props}/>
})

export const HaystackRecordEditorInner = observer(({record, additionalTiles}) => {

	const i18n = usei18n()

	const [showTiles, setShowTiles] = useState(true)

	// cache our list of tags
	const [tags, setTags] = useState(tagsOf(record))

	useEffect(() => {
		setTags(tagsOf(record))
	}, [record])

	// get our tiles and consumed tags
	const {tiles, consumedTags} = useTiles({record, tags, additionalTiles})

	// splice out consumedtags if we are showing tiles tags
	const remainingTags = useMemo(
		() =>
			showTiles ? tags.filter(tag => !consumedTags.includes(tag)) : tags,
		[record, consumedTags, showTiles]
	)

	const {markers, properties} = useMemo(() => {
		return {
			markers: remainingTags.filter(isMarkerTag),
			properties: remainingTags.filter(isNotMarkerTag),
		}
	}, [remainingTags])

	const isList = record.kind === 'List'

	const refreshState = useCallback(() => {
		setTags(isList ? [...record.value] : [...record.tags])
	}, [])

	return (
		<>
			{!isList && (
				<>
					<Container top horizontal nowrap>
						<Markers
							markers={markers}
							record={record}
							onAddNewMarkerTag={name => {
								if (record.tagsTable[name]) {
									return Modal.warning({
										content: i18n.get(`{{name}} already exists on record`),
									})
								}
								record.addTag(
									new fin.Haystack.HaystackMarker(name)
								)
								refreshState()
							}}
						/>
						<Spacer />
						<Dropdown
							trigger={['click']}
							overlay={
								<Menu>
									<Menu.Item
										onClick={() =>
											setShowTiles(!showTiles)
										}>
										Switch to{' '}
										{showTiles ? i18n.get('Advanced') : i18n.get('Basic')} view
									</Menu.Item>
								</Menu>
							}>
							<Icon type='more' style={{marginLeft: '1rem'}} />
						</Dropdown>
					</Container>
					<Divider style={{margin: '1em 0'}} />
				</>
			)}
			{showTiles && tiles.length > 0 && (
				<>
					<Container grid columns={1}>
						{tiles.map((Comp, i) => (
							<Comp
								key={`tile-${i}`}
								onRename={refreshState}
								onRemove={refreshState}
							/>
						))}
					</Container>
					<Divider style={{margin: '1em 0'}} />
				</>
			)}
			<TagTable
				tags={properties}
				record={record}
				onRemove={refreshState}
				onRename={refreshState}
			/>
			<Divider style={{margin: '1em 0'}} />
			<AddTag record={record} onAdd={refreshState} />
		</>
	)
})

export const HaystackRecordEditorModal = observer(({
	record,
	width = 480,
	recordEditorProps,
	...props
}) => {
	const title =
		props.title || record
			? `Edit "${record.displayName}" ${
					record.tagsTable.id ? `(${record.tagsTable.id})` : ''
			  }`
			: `Record Editor`
	const {save, saving, buttonLabel} = useFinCommit({record})
	return (
		<Modal
			title={title}
			width={width}
			okText={buttonLabel}
			okButtonProps={saving ? {loading: true} : null}
			onOk={save}
			bodyStyle={{padding: '1em'}}
			destroyOnClose
			{...props}>
			<HaystackRecordEditor record={record} {...recordEditorProps} />
		</Modal>
	)
})

export const HaystackRecordEditorDrawer = observer(({
	record,
	width = 480,
	recordEditorProps,
	...props
}) => {
	const i18n = usei18n()
	const title =
		props.title || record
			? `Edit "${record.displayName}" ${
					record.tagsTable.id ? `(${record.tagsTable.id})` : ''
			  }`
			: i18n.get(`Record Editor`)
	const {save, saving, buttonLabel} = useFinCommit({record})
	return (
		<Drawer
			title={title}
			width={width}
			bodyStyle={{padding: '1em'}}
			destroyOnClose
			{...props}>
			<HaystackRecordEditor record={record} {...recordEditorProps} />
			<Divider />
			<Container>
				<Spacer />
				<Button loading={saving} type='primary' block onClick={save}>
					{buttonLabel}
				</Button>
			</Container>
		</Drawer>
	)
})

