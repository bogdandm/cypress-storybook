import React from 'react'
import {Card} from '../../../antd'
import {TagTable} from '../TagTable'

export function EssentialsTile(record) {
	const consumedTags = ['id', 'dis', 'disMacro', 'name', 'navName']

	if (!record.tags.some(tag => consumedTags.includes(tag.name))) {
		return null
	}

	const {tagsTable} = record

	const render = ({onRename, onRemove}) => {
		return (
			<Card size='small'>
				<TagTable
					tags={consumedTags
						.filter(t => tagsTable[t])
						.map(tag => (tag = tagsTable[tag]))}
					record={record}
					onRename={onRename}
					onRemove={onRemove}
				/>
			</Card>
		)
	}

	return {consumedTags, render}
}
