import React from 'react'
import {Card} from '../../../antd'
import {TagTable} from '../TagTable'

export function GeoTile(record) {
	const consumedTags = record.tags.filter(tag => /geo/.test(tag.name))
	if (consumedTags.length === 0) {
		return
	}
	const render = ({onRename, onRemove}) => {
		return (
			<Card size='small'>
				<TagTable
					tags={consumedTags}
					record={record}
					onRename={onRename}
					onRemove={onRemove}
				/>
			</Card>
		)
	}
	return {consumedTags, render}
}
