import {useMemo} from 'react'
import * as builtInTiles from './tiles'
import {isFunction} from 'lodash'

const defaultTiles = Object.values(builtInTiles)

export function isMarkerTag(tag) {
	return tag.kind === 'Marker'
}

export function isNotMarkerTag(tag) {
	return tag.kind !== 'Marker'
}

/**
 * Returns a *shallow* duplicate of a tag.
 * May not properly duplicate all properties
 *
 * @param tag HaystackObject
 * @returns {HaystackObject}
 */
export function duplicateTag(tag) {
	const {name, kind, tags, value} = tag
	const dup = fin.Haystack.GenerateTag(name, kind, value)
	dup.tags = tags.slice()
	return dup
}

export function tagsOf(record) {
	const isList = record.kind === 'List'
	return isList ? record.value.slice() : record.tags.slice()
}

type Tile = (record: any) => JSX.Element

export function useTiles({record, tags, additionalTiles}): {tiles: Tile[], consumedTags: string[]} {
	return useMemo(() => {
		let newTileComps = [],
			newConsumedTags: Set<string> = new Set()
		defaultTiles.concat(additionalTiles).forEach(tile => {
			if (!isFunction(tile)) {
				if (DEBUG) {
					console.warn(
						'HaystackRecordEditor: non-function passed to additionalTiles'
					)
				}
				return
			}
			const result = tile(record)
			// Tiles can return null/undefined if they don't want
			// to display anything
			if (!result) {
				return
			}
			let {render, consumedTags} = result
			if (DEBUG && !render) {
				console.warn('Custom tile did not return a render fn: ', tile)
			}
			if (consumedTags && consumedTags.length) {
				// handle an array of tag names or HaystackObject instances
				consumedTags = consumedTags
					.map(tag =>
						tag instanceof fin.Haystack.HaystackObject
							? tag
							: record.tagsTable[tag]
					)
					.forEach(tag => newConsumedTags.add(tag))
			}
			newTileComps.push(render)
		})

		//return [newTileComps, newConsumedTags]
		return {tiles: newTileComps, consumedTags: [...newConsumedTags]}
	}, [defaultTiles, additionalTiles, tags])
}


/*
class Tile {

	constructor() {

		// checks to make sure the Tile class
		// is extended properly

	}

}

class SiteEssentialsTile extends Tile {

	filter = ['site and foo', 'foo', (tag) => tag.kind == 'ref' && tag.value  ]

	render({record}) {

		return <div>
			<h1>{record.tagsTable.dis}</h1>
		</div>

	}


}


class IGUEssentialsTile extends Tile {

	filter = 'igu'

	render({record}) {

		return <div>
			some specialized igu crap
		</div>

	}

}

*/
