import React, {useState} from 'react'
import {usei18n} from '@j2inn/utils'
import {observer} from 'mobx-react'
import classes from './style.styl'
import {Dropdown, Icon, Input, Modal, Menu, Tooltip} from '../../antd'
import {Container} from '../Container'
import {TagValue} from './TagValue'
import {TagEditor} from './TagEditor'
import classnames from 'classnames'

const nonEditableTags = ['id', 'mod']

const TagRenameDialog = observer(({tag, onRename}) => {
	const [newName, setNewName] = useState('')
	const i18n = usei18n()
	const acceptRename = () => onRename(newName, tag.name)
	// TODO i18n
	return (
		<Modal title={`Rename "${tag.name}" tag to...`} onOk={acceptRename}>
			<Input
				value={newName}
				onChange={e => setNewName(e.target.value)}
				onPressEnter={acceptRename}
				placeholder={i18n.get('New tag name...')}
				data-test-id='hre-tag-rename-input'
				autoFocus
			/>
		</Modal>
	)
})

const TagRow = observer(({tag, onRename, onRemove}) => {
	const editable = !nonEditableTags.includes(tag.name)
	const i18n = usei18n()
	const [showRenameDialog, setShowRenameDialog] = useState(false)
	return (
		<tr>
			{tag.name && (
				<td
					class={classnames(classes.tagName, tag.kind, {
						[classes.editable]: editable,
					})}>
					<Tooltip title={tag.kind}>{tag.name}</Tooltip>
					{editable && (
						<Dropdown
							trigger={['click']}
							overlay={
								<Menu>
									<Menu.Item
										onClick={() =>
											setShowRenameDialog(true)
										}>
										<Icon type='edit' /> ${i18n.get('Rename')}
									</Menu.Item>
									<Menu.Item
										onClick={() =>
											onRemove && onRemove(tag)
										}>
										<Icon type='delete' /> ${i18n.get('Delete')}
									</Menu.Item>
								</Menu>
							}>
							<Icon type='down' />
						</Dropdown>
					)}
				</td>
			)}
			<td class={classes.tagEditor}>
				{editable ? <TagEditor tag={tag} /> : <TagValue tag={tag} />}
			</td>
			{showRenameDialog && (
				<TagRenameDialog
					tag={tag}
					onRename={(newName, oldName) => {
						onRename && onRename(newName, oldName, tag)
						setShowRenameDialog(false)
					}}
				/>
			)}
		</tr>
	)
})
export function TagTable({tags, record, onRemove, onRename}) {
	if (!tags || (tags && tags.length) === 0) {
		return (
			<Container center middle>
				<em>No property tags</em>
			</Container>
		)
	}

	const onRemoveShim = (tag) => {
		record.removeTag(tag.name)
		onRemove && onRemove(tag)
	}

	const onRenameShim = (newName, oldName) => {
		const tag = record.tagsTable[oldName]
		record.removeTag(oldName)
		tag.name = newName
		record.addTag(tag)
		onRename && onRename(newName, oldName)
	}

	return (
		<table class={classes.table} data-test-id='hre-tag-table'>
			<tbody>
				{tags.map((tag, i) => (
					<TagRow
						tag={tag}
						onRemove={onRemoveShim}
						onRename={onRenameShim}
						key={i + tag.name}
					/>
				))}
			</tbody>
		</table>
	)
}
