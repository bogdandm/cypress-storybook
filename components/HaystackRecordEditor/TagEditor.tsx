import React, {useState} from 'react'
import {usei18n} from '@j2inn/utils'
import {observer} from 'mobx-react'
import HaystackTagEditor from '../HaystackTagEditor'
import {TagValue} from './TagValue'
import {Modal, Button} from '../../antd'
import {HaystackRecordEditor} from './index'
import useForceUpdate from 'use-force-update'

export const TagEditor = observer(({tag}) => {
	const {kind} = tag
	const isList = kind === 'List'
	const isListOrDict = kind === 'Dict' || kind === 'List'
	const i18n = usei18n()
	const [originalValue, setOriginalValue] = useState(
		isListOrDict ? fin.HaystackEncoder.EncodeZinc(tag) : tag.value
	)
	const forceUpdate = useForceUpdate()
	const [refModalVisible, setRefModalVisible] = useState(false)

	if (kind === 'Marker') {
		return <TagValue tag={tag} />
	}

	if (kind === 'Ref') {
		const closeRefEditor = () => setRefModalVisible(false)
		const openRefEditor = () => setRefModalVisible(true)
		return (
			<>
				<TagValue tag={tag} />
				{' '}
				<Button size='small' onClick={openRefEditor}>Select</Button>
				<Modal
					width={380}
					title={i18n.get('Edit {{obj}}', {obj: tag.name})}
					bodyStyle={{padding: '1em'}}
					footer={null}
					onCancel={closeRefEditor}
					visible={refModalVisible}>
					<HaystackTagEditor
						tag={tag}
						onChange={async value => {
							if (value) {
								const {result} = await finstack.eval(
									`readById(${value})`
								)
								tag.value = value
								tag.displayName =
									result.gridRows[0].tagsTable.dis.value
							} else {
								tag.value = null
								tag.displayName = null
							}

							setRefModalVisible(false)
							forceUpdate()
						}}
						style={{width: '100%'}}
					/>
				</Modal>
			</>
		)
	}

	if (isListOrDict) {
		return (
			<>
				<TagValue tag={tag} />
				<Modal
					width={480}
					title={`${i18n.get('Edit')} ${tag.name}`}
					destroyOnClose
					onCancel={() => {
						if (isList) {
							tag.value = originalValue
						} else {
							const grid = fin.Haystack.DeserializeHaystack(
								`ver: "3.0"\nval\n${originalValue}`
							)
							tag.tags = grid.gridRows[0].tagsTable.val.tags.slice()
						}
					}}
					onOk={() => {
						setOriginalValue(
							isListOrDict
								? fin.HaystackEncoder.EncodeZinc(tag)
								: tag.value
						)
						if (isList) {
							tag.value = tag.value.slice()
						} else {
							tag.tags = tag.tags.slice()
						}
					}}
					trigger={<Button size='small'>{i18n.get('Edit')}</Button>}>
					<HaystackRecordEditor record={tag} />
				</Modal>
			</>
		)
	}
	return (
		<HaystackTagEditor tag={tag} onChange={value => (tag.value = value)} />
	)
})
