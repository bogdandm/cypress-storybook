import React, {useState} from 'react'
import {observer} from 'mobx-react'
import {usei18n} from '@j2inn/utils'
import {Modal, Button, Input, Select} from '../../antd'
import {Container} from '../Container'
import styles from './style.styl'

const naturalSort = (a, b) => a.localeCompare(b)

const validations = [
	{
		test: s => /^[^a-z]/.test(s),
		message: 'Tag name must start with lowercase letter',
	},
	{
		test: s => s.length < 3,
		message: 'Tag name must be at least 3 characters long',
	},
	{
		test: s => /[^a-zA-Z0-9_]/.test(s),
		message: 'Tag name cannot contain any special characters',
	},
]

const addTagToRecord = ({
	record,
	newTagName,
	newTagKind,
	newTagValue,
	isList,
	i18n
}) => {
	const errors = validations
		.map(({test, message}) => (test(newTagName) ? i18n.get(message) : null))
		.filter(e => e)
	if (errors.length > 0) {
		return Modal.warning({
			title: i18n.get('Error'),
			content:
				errors.length > 1 ? (
					<ul>
						{errors.map(error => (
							<li key={error}>{error}</li>
						))}
					</ul>
				) : (
					errors[0]
				),
		})
	}
	const name = fin.Haystack.Utils.toTagName(newTagName)
	if (!isList) {
		if (!newTagValue) {
			newTagValue = '' // default to Str
		}
		if (record.tagsTable[name]) {
			return Modal.warning({
				content: `${name} ${i18n.get('already exists on record')}`,
			})
		}
	}
	let newTag
	if (newTagKind === 'Auto') {
		try {
			newTag = fin.Haystack.DeserializeHaystack(
				`ver: "3.0"\nval\n${newTagValue}`
			).gridRows[0].tags[0]
		} catch (e) {
			// could not parse, but that's fine. We'll default to a string
			try {
				newTag = fin.Haystack.DeserializeHaystack(
					`ver: "3.0"\nval\n"${newTagValue}"`
				).gridRows[0].tags[0]
			} catch (e2) {
				// If something goes wrong here we in doodoo land
				console.error(e2)
				// TODO i18n
				return Modal.error({
					content: `Could not parse ${newTagValue} into a Haystack Object`,
				})
			}
		}
	} else {
		newTag = fin.Haystack.GenerateTag(name, newTagKind)
	}
	if (isList) {
		delete newTag.name
	} else {
		newTag.name = name
	}
	if (isList) {
		record.value.push(newTag)
	} else {
		record.addTag(newTag)
	}
	return newTag
}

export const AddTag = observer(({record, onAdd}) => {
	const isList = record.kind === 'List'
	const [newTagName, setNewTagName] = useState('')
	const [newTagKind, setNewTagKind] = useState('Auto')
	const [newTagValue, setNewTagValue] = useState('')
	const i18n = usei18n()
	const add = () => {
		const newTag = addTagToRecord({
			record,
			newTagKind,
			newTagName,
			newTagValue,
			isList,
			i18n
		})
		setNewTagName(null)
		setNewTagValue(null)
		onAdd(newTag)
	}
	return (
		<Container horizontal nowrap class={styles.addTagContainer}>
			{' '}
			<Select
				size='small'
				placeholder='Kind'
				value={newTagKind}
				onChange={v => setNewTagKind(v)}
				dropdownMatchSelectWidth={false}
				style={{flex: '0 0 auto'}}>
				<Select.Option value='Auto'>Auto</Select.Option>
				{Object.values(fin.Haystack.TagKind)
					.sort(naturalSort)
					.map(kind => (
						<Select.Option key={kind}>{kind}</Select.Option>
					))}
			</Select>
			{!isList && (
				<Input
					size='small'
					placeholder='New tag name'
					value={newTagName}
					onPressEnter={add}
					onChange={e => setNewTagName(e.target.value)}
					data-test-id="new-tag-name-input"
					style={{flex: '1 1 auto'}}
				/>
			)}
			{newTagKind === 'Auto' && (
				<Input
					size='small'
					placeholder='New tag value'
					value={newTagValue}
					onPressEnter={add}
					onChange={e => setNewTagValue(e.target.value)}
					data-test-id="new-tag-value-input"
					style={{flex: '1 1 auto'}}
				/>
			)}
			<Button
				size='small'
				icon='plus'
				class={styles.newTagButton}
				type={
					(isList && newTagValue) || newTagName ? 'primary' : 'dashed'
				}
				style={
					(isList && newTagValue) || newTagName
						? null
						: {background: 'transparent', flex: '0 1 20px'}
				}
				disabled={isList ? !newTagValue : !newTagName}
				data-test-id="new-tag-add-button"
				onClick={add}>
				Add tag
			</Button>
		</Container>
	)
})
