import React from 'react'
import {storiesOf} from '@storybook/react'
import {withKnobs, select, text} from '@storybook/addon-knobs'
import {action} from '@storybook/addon-actions'
import {unescape} from 'lodash'

import {CodeEditor, modes, themes} from './CodeEditor'
import {Container} from './Container'

storiesOf('Code Editor', module)
	.addDecorator(withKnobs)
	.add('basic', () => {
		const props = {
			theme: select('Theme', Object.keys(themes), 'solarized_dark'),
			mode: select(
				'Mode',
				['javascript', ...Object.keys(modes)],
				'javascript'
			),
			onChange: action('onChange')
		}
		return <div style={{padding: '1em'}}>
			<CodeEditor {...props} />
		</div>
	})
	.add('beautifier', () => {
		const props = {
			theme: select('Theme', Object.keys(themes), 'solarized_dark'),
			mode: select(
				'Mode',
				['javascript', ...Object.keys(modes)],
				'xml'
			),
			onChange: action('onChange'),
			value: text('code', '<foo><bar><baz><bing>bob</bing></baz></bar></foo>')
		}
		const value = unescape(props.value)
		return <div style={{padding: '1em'}}>
			Input: <code><pre>{value}</pre></code>
			Formatted in CodeEditor:
			<CodeEditor {...props} value={value} beautify/>
		</div>
	})

import {ColorPicker} from './ColorPicker'

storiesOf('Color Picker', module).add('basic', () => {
	return <ColorPicker onChange={action('onChange')} />
})

import {RelativeTimePicker} from './RelativeTimePicker'

storiesOf('Relative Time Picker', module).add('basic', () => {
	return <RelativeTimePicker onChange={action('onChange')} />
})

const colors = [
	'#9db7fb',
	'#bb9dfb',
	'#f49dfb',
	'#fb9dca',
	'#fba99d',
	'#fbe19d',
	'#ddfb9d',
	'#a4fb9d',
	'#9dfbce',
	'#9deffb',
]
const Items = ({styles}) => colors.map(color => <div key={color} style={{background: color, ...styles}}></div>)
function ContainerExample({examples, itemStyles = {width: 20, height: 20}}) {
	return <>
		<h1 class='title'>Container</h1>

		<p className='text'>
			Container makes flex and css grid layouts dead simple through some easy-to-remember
			keywords passed as boolean props.
		</p>

		<hr/>
		<h2 className='title'>Examples:</h2>
		<Container grid colWidth="300px" style={{padding: 25}} gap='5rem'>
			{examples.map((example, i) => {
				const isObject = typeof example !== 'string'
				const props = isObject ? example : example.split(' ').reduce((a, c) => {
					a[c] = true
					return a
				}, {})
				const exampleText = `<Container ${isObject ? Object.keys(example).map(c => {
					if(example[c] !== true) {
						if(typeof example[c] === 'string') {
							c = `${c}="${example[c]}"`
						} else {
							c = `${c}={${example[c]}}`
						}
					}
					return c
				}).join(' ') : example}>`
				return <Container center key={`example-${i}`} style={{width: 300}}>
					<code style={{maxWidth: '100%'}}><pre>{exampleText}</pre></code>
					<Container {...props} style={{width: '100%', height: 150, border: '1px dashed #ccc'}}>
						<Items styles={itemStyles}/>
					</Container>
				</Container>
			})}
		</Container>
	</>
}
storiesOf('Container', module).add('horizontal (default)', () => {
	return <ContainerExample examples={[
		'left',
		'center',
		'right',
		'top',
		'middle',
		'bottom',
		'left top',
		'center middle',
		'right bottom',
		'center middle evenly',
		'center middle loose',
		'center middle tight',
	]}/>
})
storiesOf('Container', module).add('vertical', () => {
	return <ContainerExample examples={[
		'vertical top left',
		'vertical center middle',
		'vertical right bottom',
	]}/>
})
storiesOf('Container', module).add('grid', () => {
	return <>
		<style>
			{`
			.expand {
				animation: expand 2s infinite alternate;
				width: 300px !important;
			}
			@keyframes expand {
				0% {
					width: 300px;
				}
				100% {
					width: 200px;
				}
			}
			`}
		</style>
		<ContainerExample examples={[
			{grid: true, gap: 5},
			{grid: true, columns: 3},
			{grid: true, colWidth: 40},
			{grid: true, bottom: true, colWidth: 40},
			{grid: true, left: true, colWidth: 20},
			{grid: true, bottom: true, right: true, colWidth: 20},
			{grid: true, gap: '0.5rem 5rem'},
			{grid: true, colWidth: 40, className: 'expand'},
		]}
		itemStyles={{height: 20}}/>
	</>
})

